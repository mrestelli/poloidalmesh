## Copyright (C) 2016  Marco Restelli
##
## This file is part of: Poloidal Mesh Designer
##
## Poloidal Mesh is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3 of the
## License, or (at your option) any later version.
##
## Poloidal Mesh is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with the Poloidal Mesh source; If not, see
## <http://www.gnu.org/licenses/>.
##
## author: Marco Restelli                   <marco.restelli@gmail.com>


import sys
import numpy as np
import re
from PyQt4 import QtCore, QtGui
# Import the console machinery
from qtconsole.rich_ipython_widget import RichJupyterWidget
from qtconsole.inprocess import QtInProcessKernelManager
# Import the matplotlib machinery
from matplotlib.backends.backend_qt4agg import \
  FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure

from mesh_viewer_mpl_backend import Mesh

"""
QT mesh GUI module
"""

# ----------------------------------------------------------------------

float_re = r'[+-]?(\d+\.?\d*|\d*\.?\d+)(e[+-]?\d+|E[+-]?\d+)?'
float_array_re = \
  r'\s*\[?\s*'+float_re+r'(\s*,\s*'+float_re+r')*'+r'\s*\]?\s*'

def integer_validator(parent):
  return QtGui.QRegExpValidator( QtCore.QRegExp( \
    r'^[+-]?\d*' ) , parent )

def pinteger_validator(parent):
  return QtGui.QRegExpValidator( QtCore.QRegExp( \
    r'^[1-9]\d*' ) , parent )

def float_validator(parent):
  return QtGui.QRegExpValidator( QtCore.QRegExp( \
    r'^'+float_re ) , parent )

def float_list_validator(parent):
  return QtGui.QRegExpValidator( QtCore.QRegExp( \
    r'^'+float_array_re ) , parent )

def fname_validator(parent):
  # TODO: this does not exclude leading digits
  return QtGui.QRegExpValidator( QtCore.QRegExp( \
    r'\w+' ) , parent )

# ----------------------------------------------------------------------

class QtConfig():
  """
  A class to wrap the Qt QSettings machinery to store and reconver
  configuration options.
  """

  def __init__(self,mesh):
    self.mesh = mesh

  def save_config(self):

    from mesh_viewer_mpl_backend import Vertex, Side

    settings = QtCore.QSettings()

    settings.setValue('Vertexes/marker_size', Vertex.markersize  )
    settings.setValue('Vertexes/marker'     , Vertex.marker      )
    settings.setValue('Vertexes/marker_psi' , Vertex.marker_psi  )
    settings.setValue('Vertexes/marker_lock', Vertex.marker_lock )

    settings.setValue('Sides/line_width' , Side.linewidth )

    settings.setValue("Axes/font_size" , self.mesh.get_fonts())

  def read_config(self):

    from mesh_viewer_mpl_backend import Vertex, Side

    settings = QtCore.QSettings()

    vms = settings.value("Vertexes/marker_size")
    if vms is not None: Vertex.markersize  = float( vms )
    vm  = settings.value("Vertexes/marker")
    if vm is not None:  Vertex.marker      = str(   vm  )
    vmp = settings.value("Vertexes/marker_psi")
    if vmp is not None: Vertex.marker_psi  = str(   vmp )
    vml = settings.value("Vertexes/marker_lock")
    if vml is not None: Vertex.marker_lock = str(   vml )

    slw = settings.value("Sides/line_width")
    if slw is not None: Side.linewidth = float( slw )

    afs = settings.value("Axes/font_size")
    if afs is not None: self.mesh.set_fonts( afs )

# ----------------------------------------------------------------------

class QtConsole( RichJupyterWidget ):

  def __init__(self,*args,**kwargs):
    super().__init__(*args,**kwargs)
    # kernel manager
    kernel_manager = QtInProcessKernelManager()
    kernel_manager.start_kernel()
    kernel_manager.kernel.gui = 'qt4'
    self.kernel_manager = kernel_manager
    # kernel client
    kernel_client = self._kernel_manager.client()
    kernel_client.start_channels()
    self.kernel_client = kernel_client
    # add a method to close the the app when exiting the terminal
    self.exit_requested.connect(self.shutdown_all)

  def set_callbacks(self):
    pass

  def activate(self):
    pass

  def pushVariables(self,variableDict):
    self.kernel_manager.kernel.shell.push(variableDict)

  def pullVariable(self,var_name):
    return self.kernel_manager.kernel.shell.ns_table[ \
                                                 "user_local"][var_name]

  def shutdown_all(self):
    print("Closing...")
    self.kernel_client.stop_channels()
    self.kernel_manager.shutdown_kernel()
    QtGui.QApplication.instance().exit()
    print("Done!")

# ----------------------------------------------------------------------

class MatplMeshWindow( QtGui.QMainWindow ):

  def __init__(self,*args):
    super().__init__(*args)
    # 1) generate the figure
    fig = Figure()
    canvas = FigureCanvasQTAgg(fig)
    # this is necessary to accept focus and receive key_press evts.
    canvas.setFocusPolicy( QtCore.Qt.StrongFocus )
    # 1.1) store it
    self.fig    = fig
    self.canvas = canvas
    # 2) insert the figure in the container window
    self.setCentralWidget( self.canvas )
    # 3) add a toolbar for the figure
    self.toolbar = NavigationToolbar2QT(self.canvas,self)
    self.addToolBar(self.toolbar)
    # 3.1) keep a reference to some toolbar actions
    self.pan_action  = self.toolbar.actions()[4]
    self.zoom_action = self.toolbar.actions()[5]
    # 4) MatPlotLib mesh backend
    self._mesh = Mesh( self.fig, self.canvas )

  def set_callbacks(self, \
      mouse_click_callback , mouse_click_drag_callback, \
      object_pick_callback , mouse_scroll_callback,     \
      key_press_callback   , activated_toolbar_callback ):
    # MatPlotLib back-end callbacks
    self._mesh.set_callbacks( \
      mouse_click_callback , mouse_click_drag_callback, \
      object_pick_callback , mouse_scroll_callback,      \
      key_press_callback )
    # Front-end callbacks
    self.activated_toolbar_callback = activated_toolbar_callback
    self.pan_action.toggled.connect( self.notify_activated_toolbar)
    self.zoom_action.toggled.connect(self.notify_activated_toolbar)

  def activate(self,Psi):
    self._mesh.activate(Psi)

  def notify_activated_toolbar(self):
    self.activated_toolbar_callback( \
      self.pan_action.isChecked() or self.zoom_action.isChecked() )

  # Delegated methods
  def plot_psi_countours(self,X,Y,psi_levs,style):
    return self._mesh.plot_psi_countours(X,Y,psi_levs,style)

  def remove_psi_contour(self,artist):
    return self._mesh.remove_psi_contour(artist)

# ----------------------------------------------------------------------

class HelpBrowser( QtGui.QMainWindow ):

  def __init__(self,close_this_callback,*args,**kwargs):
    super().__init__(*args,**kwargs)
    # 1) Generate the text browser
    text_browser = QtGui.QTextBrowser()
    #text_browser.setHtml("""Uij mnh <b>òàè Ψ υ</b> Κ × Ω""")
    text_browser.setSource(QtCore.QUrl("./doc.html"))
    self.text_browser = text_browser
    # 2) Generate the OK button
    close_button = QtGui.QPushButton("Close")
    close_button.clicked.connect( close_this_callback )
    self.close_button = close_button

    # 2) Create the layout
    main_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
    main_splitter.addWidget( text_browser )
    main_splitter.addWidget( close_button )
    self.setCentralWidget( main_splitter )

# ----------------------------------------------------------------------

class VertexEditor( QtGui.QGroupBox ):

  def __init__(self,*args):
    super().__init__(*args)

    layout = QtGui.QFormLayout()

    # Vertex label
    vlabel = QtGui.QLabel()
    layout.addRow( vlabel )
    self.vlabel = vlabel

    # apply button
    vapply = QtGui.QPushButton("Apply")
    vapply.clicked.connect( self.set_vdata_handler )

    # Vertex coordinates
    vxy = {}
    for i,s in enumerate(["x","y"]):
      vxy[s] = QtGui.QLineEdit()
      vxy[s].setAlignment(QtCore.Qt.AlignRight)
      vxy[s].setValidator(float_validator(self))
      vxy[s].returnPressed.connect( vapply.animateClick )
      layout.addRow(s, vxy[s] )
    self.vxy = vxy

    # psi editor
    vpsi = {}
    vpsi["cb"] = QtGui.QCheckBox()
    vpsi["le"] = QtGui.QLineEdit()
    vpsi["le"].setAlignment(QtCore.Qt.AlignRight)
    vpsi["le"].setValidator(float_validator(self))
    vpsi["le"].returnPressed.connect( vapply.animateClick )
    vpsi["cb"].toggled.connect( vpsi["le"].setEnabled )
    row = QtGui.QHBoxLayout()
    row.addWidget(vpsi["cb"])
    row.addWidget(vpsi["le"])
    layout.addRow("Ψ", row )
    self.vpsi = vpsi

    # locked editor
    vlocked = QtGui.QCheckBox()
    layout.addRow("lock", vlocked )
    self.vlocked = vlocked

    layout.addRow( vapply )
    self.vapply = vapply

    self.setTitle("Vertex editor")
    self.setLayout(layout)

  def set_callbacks(self, set_vdata_callback , return_focus ):
    self.set_vdata_callback = set_vdata_callback
    self.return_focus = return_focus

  def activate(self):
    # toggle the checkboxes to False making sure the signal is emitted
    for cb in ( self.vpsi["cb"] , self.vlocked ):
      cb.setChecked(True);
      cb.setChecked(False);
    self.clear_vdata()

  def update_vdata(self,vdata):
    self.update_vertex_id(vdata["id"])
    for xcoord,xi in zip(["x","y"],vdata["xy"]):
      self.vxy[xcoord].setText(str(xi))
    if vdata["psi"] is not None:
      self.vpsi["cb"].setChecked(True)
      self.vpsi["le"].setText(str(vdata["psi"]))
    else:
      self.vpsi["cb"].setChecked(False)
    self.vlocked.setChecked(vdata["lock"])
    self.setEnabled(True)

  def clear_vdata(self):
    self.update_vertex_id(None)
    for xy_le in self.vxy.values():
      xy_le.clear()
    self.vpsi["cb"].setChecked(False)
    self.vlocked.setChecked(False)
    self.setEnabled(False)

  def get_vdata(self):
    # Get data from QLineEdit objects and make a dictionary
    to_float = lambda s: float(s) if s != '' else None
    return { \
    "xy" : [to_float(self.vxy[s].text()) for s in ("x","y")] , \
   "psi" :  float(self.vpsi["le"].text()) \
                 if self.vpsi["cb"].isChecked()==1 else None , \
  "lock" : 1 if self.vlocked.isChecked() else 0 , \
      }

  def set_vdata_handler(self):
    # Gather the data in the vertex editor and communicate them
    self.set_vdata_callback( self.get_vdata() )
    self.return_focus()

  def update_vertex_id(self,vid):
    if vid is None:
      self.vlabel.setText( "vertex id: ****" )
    else:
      self.vlabel.setText( "vertex id: %d" % vid )

# ----------------------------------------------------------------------

class SideEditor( QtGui.QGroupBox ):

  def __init__(self,*args):
    super().__init__(*args)

    layout = QtGui.QFormLayout()

    # Side label
    slabel = QtGui.QLabel()
    layout.addRow( slabel )
    self.slabel = slabel

    # apply button
    sapply = QtGui.QPushButton("Apply")
    sapply.clicked.connect( self.set_sdata_handler )

    # mu editor
    smu = {}
    smu["cb"] = QtGui.QCheckBox()
    smu["le"] = QtGui.QLineEdit()
    smu["le"].setAlignment(QtCore.Qt.AlignRight)
    smu["le"].setValidator(pinteger_validator(self))
    smu["le"].returnPressed.connect( sapply.animateClick )
    smu["cb"].toggled.connect( smu["le"].setEnabled )
    row = QtGui.QHBoxLayout()
    row.addWidget(smu["cb"])
    row.addWidget(smu["le"])
    layout.addRow("μ", row )
    self.smu = smu

    layout.addRow( sapply )
    self.sapply = sapply

    self.setTitle("Side editor")
    self.setLayout(layout)

  def set_callbacks(self, set_sdata_callback , return_focus ):
    self.set_sdata_callback = set_sdata_callback
    self.return_focus = return_focus

  def activate(self):
    # toggle the checkboxes to False making sure the signal is emitted
    for cb in ( self.smu["cb"] , ):
      cb.setChecked(True);
      cb.setChecked(False);
    self.clear_sdata()

  def update_sdata(self,sdata):
    self.update_side_id(sdata["id"])
    if sdata["mu"] is not None:
      self.smu["cb"].setChecked(True)
      self.smu["le"].setText(str(sdata["mu"]))
    else:
      self.smu["cb"].setChecked(False)
    self.setEnabled(True)

  def clear_sdata(self):
    self.update_side_id(None)
    self.smu["cb"].setChecked(False)
    self.setEnabled(False)

  def set_sdata_handler(self):
    # Gather the data in the vertex editor and communicate them
    self.set_sdata_callback( { \
    "mu" : int(self.smu["le"].text()) \
            if self.smu["cb"].isChecked()==1 else None \
      })
    self.return_focus()

  def update_side_id(self,sid):
    if sid is None:
      self.slabel.setText( "side id: ****" )
    else:
      self.slabel.setText( "side id: %d" % sid )

# ----------------------------------------------------------------------

class PsiContourLimits( QtGui.QGroupBox ):

  def __init__(self,*args):
    super().__init__(*args)

    self._X = None
    self._Y = None

    layout = QtGui.QFormLayout()

    # apply button
    psiapply = QtGui.QPushButton("Apply")
    psiapply.clicked.connect( self.set_psi_lim_handler )

    # Limits of the contous plot
    psi_lim = {}
    for var in ("x","y"):
      psi_lim[var] = ( QtGui.QLineEdit() , QtGui.QLineEdit() )
      row = QtGui.QHBoxLayout()
      for le in psi_lim[var]:
        le.setAlignment(QtCore.Qt.AlignRight)
        le.setValidator(float_validator(self))
        le.returnPressed.connect( psiapply.animateClick )
        row.addWidget( le )
      layout.addRow(var+" lim.", row )
    self.psi_lim = psi_lim

    layout.addRow( psiapply )
    self.psiapply = psiapply

    self.setTitle("Ψ contour limits")
    self.setLayout(layout)

  def set_callbacks(self):
    pass

  def activate(self):
    pass

  @property
  def XY(self):
    if (self._X is not None) and (self._Y is not None):
      return (self._X,self._Y)
    else:
      return None

  def set_psi_lim_handler(self):

    # Define the mesh for the countours
    lim = { "x":[None,None] , "y":[None,None] }
    for var in ("x","y"):
      for i,lr in enumerate(self.psi_lim[var]): # left and right bounds
        xy = lr.text()
        if xy is "": return # invalid input
        lim[var][i] = float(xy)
      if lim[var][0] >= lim[var][1]: return # invalid range
    X,Y = np.meshgrid( np.linspace( lim["x"][0],lim["x"][1] , 500 ) , \
                       np.linspace( lim["y"][0],lim["y"][1] , 500 ) )
    self._X = X
    self._Y = Y

    # Echo the values
    for var in ("x","y"):
      for lr,xi in zip(self.psi_lim[var],lim[var]):
        lr.setText(str(xi))

# ----------------------------------------------------------------------

class PsiContourController( QtGui.QGroupBox ):

  sep_style = {'colors':('k',),"linewidths":(1,),"zorder":-10}

  def __init__(self,psi_lims,*args):
    super().__init__(*args)

    self.psi_lims = psi_lims

    layout = QtGui.QFormLayout()

    # apply button
    psiapply = QtGui.QPushButton("Apply")
    psiapply.clicked.connect( self.set_psi_conts_handler )

    # Get values from selected vertexes
    get_psi_sep  = QtGui.QPushButton("Get Ψ sep.")
    get_psi_levs = QtGui.QPushButton("Get Ψ levs.")
    get_psi_sep.setToolTip( \
    "Set Ψ sep. using the value of the vertex in the vertex editor" )
    get_psi_levs.setToolTip( \
    "Add the Ψ value of the vertex in the vertex editor to Ψ levs." )
    row = QtGui.QHBoxLayout()
    row.addWidget(get_psi_sep )
    row.addWidget(get_psi_levs)
    layout.addRow( row )

    # Separatrix
    psi_sep = QtGui.QLineEdit()
    psi_sep.setAlignment(QtCore.Qt.AlignRight)
    psi_sep.setValidator(float_validator(self))
    psi_sep.returnPressed.connect( psiapply.animateClick )
    layout.addRow("Ψ sep.", psi_sep )
    self.psi_sep = psi_sep
    self.sep_artist = None
    self.vals_artist = None

    get_psi_sep.clicked.connect(  lambda :         \
      self.psi_sep.setText(str(                    \
            self.get_dict_selected_vert()["psi"] ) \
      ) )
    get_psi_levs.clicked.connect( lambda :         \
      self.psi_levs__float_list_to_text(           \
        self.psi_levs__text_to_float_list()        \
        + [ self.get_dict_selected_vert()["psi"] ] \
      ) )

    # Other contours
    psi_levs = QtGui.QLineEdit()
    psi_levs.setAlignment(QtCore.Qt.AlignLeft)
    psi_levs.setValidator(float_list_validator(self))
    psi_levs.returnPressed.connect( psiapply.animateClick )
    layout.addRow("Ψ levs.", psi_levs )
    self.psi_levs = psi_levs

    layout.addRow( psiapply )
    self.psiapply = psiapply

    self.setTitle("Ψ contour editor")
    self.setLayout(layout)

  def set_callbacks(self, mv_bkend, return_focus, \
                    get_dict_selected_vert ):
    self.mv_bkend               = mv_bkend
    self.return_focus           = return_focus
    self.get_dict_selected_vert = get_dict_selected_vert

  def activate(self):
    pass

  def psi_levs__text_to_float_list(self):
    # The list is sorted and duplicates are eliminated
    psi_levs_text = self.psi_levs.text()
    # eliminate all whitespaces and parentheses
    psi_levs_text = re.sub(r'\s+','',psi_levs_text)
    psi_levs_text = re.sub(r'[\[\]]+','',psi_levs_text)
    return sorted({ float(lev) for lev in psi_levs_text.split(",") \
                    if lev != "" })

  def psi_levs__float_list_to_text(self,psi_levs_float_list):
    # Duplicates are removed and the list is sorted
    self.psi_levs.setText(str(sorted(set(psi_levs_float_list))))

  def set_psi_conts_handler(self):

    # Get the mesh for the countours
    if self.psi_lims.XY is None: return
    X,Y = self.psi_lims.XY

    # Plot the separatrix
    try: # can fail because of None or previous errors - does not matter
      self.mv_bkend.remove_psi_contour(self.sep_artist)
    except Exception: pass
    psi_sep = self.psi_sep.text()
    if psi_sep is not "":
      psi_sep = float(psi_sep)
      self.sep_artist = self.mv_bkend.plot_psi_countours( \
        X,Y,(psi_sep,),self.sep_style )
      self.psi_sep.setText(str(psi_sep))
    else:
      psi_sep = -np.inf # used later to choose the colors
      self.sep_artist = None
    
    # Plot the required countours
    try:
      self.mv_bkend.remove_psi_contour(self.vals_artist)
    except Exception: pass
    psi_levs_float_list = self.psi_levs__text_to_float_list()
    if psi_levs_float_list: # list in not empty
      self.vals_artist = self.mv_bkend.plot_psi_countours( \
        X,Y,psi_levs_float_list, { \
         "colors":[ "r" if l > psi_sep else             \
                    "b" for l in psi_levs_float_list] , \
         "linewidths":(1,),"zorder":-10 } )
    else:
      self.vals_artist = None
    # Echo the values
    self.psi_levs__float_list_to_text(psi_levs_float_list)

    self.return_focus()

  def react2_notified_Psi_change(self,msg):
    # redraw all the contours
    self.set_psi_conts_handler()

# ----------------------------------------------------------------------

class PsiNewVertex( QtGui.QGroupBox ):

  vpsi_style = {"colors":('g',),"linewidths":(3,),"alpha":0.4}

  def __init__(self,psi_lims,*args):
    super().__init__(*args)

    self.psi_lims = psi_lims

    layout = QtGui.QFormLayout()

    # apply button
    psiapply = QtGui.QPushButton("Apply")
    psiapply.clicked.connect( self.set_psi_vert_handler )

    # psi editor
    vpsi = {}
    vpsi["cb"] = QtGui.QCheckBox()
    vpsi["le"] = QtGui.QLineEdit()
    vpsi["le"].setAlignment(QtCore.Qt.AlignRight)
    vpsi["le"].setValidator(float_validator(self))
    vpsi["le"].returnPressed.connect( psiapply.animateClick )
    vpsi["cb"].toggled.connect( vpsi["le"].setEnabled )
    row = QtGui.QHBoxLayout()
    row.addWidget(vpsi["cb"])
    row.addWidget(vpsi["le"])
    layout.addRow("Ψ<sub>vert</sub>", row )
    self.vpsi = vpsi
    self.vpsi_artist = None

    layout.addRow( psiapply )
    self.psiapply = psiapply

    self.setTitle("Ψ selected && new vertexes")
    self.setLayout(layout)

  def set_callbacks(self, mv_bkend, \
      set_psi_callback, unset_psi_callback, return_focus ):
    self.mv_bkend = mv_bkend
    self.set_psi_callback   = set_psi_callback
    self.unset_psi_callback = unset_psi_callback
    self.return_focus = return_focus

  def activate(self):
    for cb in ( self.vpsi["cb"] , ):
      cb.setChecked(True);
      cb.setChecked(False);

  def set_psi_vert_handler(self):

    # Callback
    if self.vpsi["cb"].isChecked()==1:
      vpsi = self.vpsi["le"].text()
      vpsi = float(vpsi)
      self.vpsi["le"].setText(str(vpsi))
      self.set_psi_callback(vpsi)
    else:
      self.unset_psi_callback()

    # Get the mesh for the countours
    if self.psi_lims.XY is None: return
    X,Y = self.psi_lims.XY

    # Plot the selected contour
    try:
      self.mv_bkend.remove_psi_contour(self.vpsi_artist)
    except Exception: pass
    if self.vpsi["cb"].isChecked()==1:
      self.vpsi_artist = self.mv_bkend.plot_psi_countours( \
        X,Y,(vpsi,),self.vpsi_style )
    else:
      self.vpsi_artist = None

    self.return_focus()

  def react2_notified_Psi_change(self,msg):
    # redraw the contour
    self.set_psi_vert_handler()

# ----------------------------------------------------------------------

class LockUnlockVerts( QtGui.QGroupBox ):

  def __init__(self,*args):
    super().__init__(*args)

    layout = QtGui.QFormLayout()

    # lock/unlock buttons
    lock   = QtGui.QPushButton("Lock")
    unlock = QtGui.QPushButton("Unlock")
    lock.clicked.connect(   self.lock_verts_handler   )
    unlock.clicked.connect( self.unlock_verts_handler )

    row = QtGui.QHBoxLayout()
    row.addWidget(lock  )
    row.addWidget(unlock)
    layout.addRow( row )

    self.lock   = lock
    self.unlock = unlock

    self.setTitle("Lock selected vertexes")
    self.setLayout(layout)

  def set_callbacks(self, \
      lock_verts_callback, unlock_verts_callback, return_focus ):
    self.lock_verts_callback   = lock_verts_callback
    self.unlock_verts_callback = unlock_verts_callback
    self.return_focus = return_focus

  def activate(self):
    pass

  def lock_verts_handler(self):
    self.lock_verts_callback()
    self.return_focus()

  def unlock_verts_handler(self):
    self.unlock_verts_callback()
    self.return_focus()

# ----------------------------------------------------------------------

class SetUnsetMu( QtGui.QGroupBox ):

  def __init__(self,*args):
    super().__init__(*args)

    layout = QtGui.QFormLayout()

    # apply button
    muapply = QtGui.QPushButton("Apply")
    muapply.clicked.connect( self.set_mu_side_handler )

    smu = {}
    smu["cb"] = QtGui.QCheckBox()
    smu["le"] = QtGui.QLineEdit()
    smu["le"].setAlignment(QtCore.Qt.AlignRight)
    smu["le"].setValidator(pinteger_validator(self))
    smu["le"].returnPressed.connect( muapply.animateClick )
    smu["cb"].toggled.connect( smu["le"].setEnabled )
    row = QtGui.QHBoxLayout()
    row.addWidget(smu["cb"])
    row.addWidget(smu["le"])
    layout.addRow("μ<sub>side</sub>", row )
    self.smu = smu

    layout.addRow( muapply )
    self.muapply = muapply

    self.setTitle("Set μ, selected sides")
    self.setLayout(layout)

  def set_callbacks(self, \
      set_mu_callback, unset_mu_callback, return_focus ):
    self.set_mu_callback   = set_mu_callback
    self.unset_mu_callback = unset_mu_callback
    self.return_focus = return_focus

  def activate(self):
    for cb in ( self.smu["cb"] , ):
      cb.setChecked(True);
      cb.setChecked(False);

  def set_mu_side_handler(self):
    if self.smu["cb"].isChecked()==1:
      mu = int(self.smu["le"].text())
      self.smu["le"].setText(str(mu))
      self.set_mu_callback(mu)
    else:
      self.unset_mu_callback()
    self.return_focus()

# ----------------------------------------------------------------------

class MeshOptimize( QtGui.QGroupBox ):

  def __init__(self,*args):
    super().__init__(*args)

    layout = QtGui.QFormLayout()

    # apply button
    optapply = QtGui.QPushButton("Apply")
    optapply.clicked.connect( self.optapply_handler )

    omega = QtGui.QSlider(QtCore.Qt.Horizontal)
    self.slider_scale = 1e6 # to emulate a continuous value
    omega.setMinimum(1)
    omega.setMaximum(1.1*self.slider_scale)
    layout.addRow("ω", omega )
    self.omega = omega

    niter = QtGui.QSlider(QtCore.Qt.Horizontal)
    niter.setMinimum(1)
    niter.setMaximum(25)
    layout.addRow("N<sub>iter</sub>", niter )
    self.niter = niter

    label = QtGui.QLabel()
    omega.valueChanged.connect( self.update_label )
    niter.valueChanged.connect( self.update_label )
    layout.addRow( label )
    self.label = label

    dxlabel = QtGui.QLabel()
    layout.addRow( dxlabel )
    self.dxlabel = dxlabel

    layout.addRow( optapply )
    self.optapply = optapply

    self.setTitle("Optimize mesh")
    self.setLayout(layout)

  def set_callbacks(self, mesh_optimize_callback, return_focus ):
    self.mesh_optimize_callback = mesh_optimize_callback
    self.return_focus = return_focus

  def activate(self):
    self.omega.setValue(1.0*self.slider_scale)
    self.niter.setValue(1)
  
  def update_label(self,_):
    self.label.setText( \
      "ω = "+("%8.6f"%(self.omega.value()/self.slider_scale))+ " — " +\
      "N<sub>iter</sub> = "+("%3d"%self.niter.value()) )

  def optapply_handler(self):
    dxmax = self.mesh_optimize_callback(       \
        self.omega.value()/self.slider_scale , \
        self.niter.value() )
    self.dxlabel.setText( \
      "‖∆x‖<sub>max</sub> = "+("%7.1e"%dxmax) )
    self.return_focus()

# ----------------------------------------------------------------------

class MeshRefine( QtGui.QGroupBox ):

  def __init__(self,*args):
    super().__init__(*args)

    layout = QtGui.QFormLayout()

    # apply button
    refapply = QtGui.QPushButton("Apply")
    refapply.clicked.connect( self.refapply_handler )

    layout.addRow( refapply )
    self.refapply = refapply

    label = QtGui.QLabel()
    layout.addRow( label )
    self.label = label

    self.setTitle("Refine mesh")
    self.setLayout(layout)

  def set_callbacks(self, mesh_refine_callback, return_focus ):
    self.mesh_refine_callback = mesh_refine_callback
    self.return_focus = return_focus

  def activate(self):
    pass

  def refapply_handler(self):
    nv,ns,ne = self.mesh_refine_callback()
    self.label.setText( \
        "N<sub>vert</sub>: " + ("%6d"%nv) + \
      "; N<sub>side</sub>: " + ("%6d"%ns) + \
      "; N<sub>elem</sub>: "+("%6d"%ne) )
    self.return_focus()

# ----------------------------------------------------------------------

class MeshGUI( QtGui.QMainWindow ):

  def __init__(self,*args):
    super().__init__(*args)

    # General Qt config
    QtCore.QCoreApplication.setOrganizationName("PoloidalMesh");
    QtCore.QCoreApplication.setApplicationName("PoloidalMesh");

    # Build the GUI
    self.createComponents(*args)
    self.createMenu()
    self.createLayout()
    
  def set_callbacks(self, \
      mouse_click_callback , mouse_click_drag_callback , \
      object_pick_callback , mouse_scroll_callback     , \
      key_press_callback   , change_gui_mode_callback  , \
      set_psi_callback,    unset_psi_callback          , \
      lock_verts_callback, unlock_verts_callback       , \
      set_mu_callback,     unset_mu_callback           , \
      mesh_optimize_callback,                            \
      mesh_refine_callback,                              \
      set_vdata_callback, set_sdata_callback ):
    # MatPlotLib backend
    self.mv_bkend.set_callbacks( \
      mouse_click_callback , mouse_click_drag_callback , \
      object_pick_callback , mouse_scroll_callback     , \
      key_press_callback   ,                             \
      self.react2_notified_active_toolbar_change )
    self.change_gui_mode_callback = change_gui_mode_callback
    # Jupyter console
    self.jpt_cons.set_callbacks()
    # Main window
    # 1) tab
    self.psi_lims.set_callbacks()
    self.psi_contr.set_callbacks( self.mv_bkend , \
      self.mv_bkend.activateWindow ,              \
      self.vertex_editor.get_vdata )
    self.psi_new_v.set_callbacks( self.mv_bkend , \
      set_psi_callback, unset_psi_callback      , \
      self.mv_bkend.activateWindow )
    # 2) tab
    self.lock_vertexes.set_callbacks( lock_verts_callback, \
      unlock_verts_callback, self.mv_bkend.activateWindow )
    self.mu_sides.set_callbacks( set_mu_callback, \
      unset_mu_callback, self.mv_bkend.activateWindow )
    self.mesh_optimize.set_callbacks( \
      mesh_optimize_callback, self.mv_bkend.activateWindow )
    self.mesh_refine.set_callbacks( \
      mesh_refine_callback, self.mv_bkend.activateWindow )
    # 3) tab
    self.vertex_editor.set_callbacks( set_vdata_callback , \
      self.mv_bkend.activateWindow )
    self.side_editor.set_callbacks(   set_sdata_callback , \
      self.mv_bkend.activateWindow )

  def activate(self,Psi,setPsi):
    # Activate the MatPlotLib backend
    self.mv_bkend.activate(Psi)
    # Set-up the Jupyter console
    self.jpt_cons.activate()
    self.jpt_cons.pushVariables( { \
      "mesh_viewer_mpl_backend":self.mv_bkend._mesh , \
                      "MeshGUI":self } )
    # Activate the main window
    self.mode_control.buttons["vertex"].setChecked(True)
    self.previous_mode = self.mode_control.buttons["vertex"]
    self.psi_lims.activate()
    self.psi_contr.activate()
    self.psi_new_v.activate()
    self.lock_vertexes.activate()
    self.mu_sides.activate()
    self.mesh_optimize.activate()
    self.mesh_refine.activate()
    self.vertex_editor.activate()
    self.side_editor.activate()

  def createComponents(self,*args):
    # Open the mesh viewer in a dedicated window
    mv_bkend = MatplMeshWindow(self,*args)
    mv_bkend.show()
    self.mv_bkend = mv_bkend
    # Open the Jupyter console in a dedicated window
    jpt_cons = QtConsole(self,*args)
    jpt_cons_w = QtGui.QMainWindow(self,*args)
    jpt_cons_w.setCentralWidget(jpt_cons)
    jpt_cons_w.show()
    self.jpt_cons   = jpt_cons
    self.jpt_cons_w = jpt_cons_w
    # Main window
    # 1) mode control
    self.make_mode_control()
    # 2) Psi setup
    self.psi_lims  = PsiContourLimits(                  self,*args)
    self.psi_contr = PsiContourController(self.psi_lims,self,*args)
    self.psi_new_v = PsiNewVertex(        self.psi_lims,self,*args)
    # 3) Mesh
    self.lock_vertexes = LockUnlockVerts(self,*args)
    self.mu_sides      = SetUnsetMu(self,*args)
    self.mesh_optimize = MeshOptimize(self,*args)
    self.mesh_refine   = MeshRefine(self,*args)
    # 4.1) vertex editor
    self.vertex_editor = VertexEditor(self,*args)
    # 4.2) side editor
    self.side_editor = SideEditor(self,*args)

  def createMenu(self):
    toolbar     = self.menuBar()
    self.toolbar = toolbar
    # Preferences menu
    config_menu = toolbar.addMenu('Preferences')
    self.config = QtConfig( self.mv_bkend._mesh )
    save_config_action = QtGui.QAction("Save preferences",self)
    save_config_action.triggered.connect(self.config.save_config)
    read_config_action = QtGui.QAction("Load preferences",self)
    read_config_action.triggered.connect(self.config.read_config)
    config_menu.addAction( save_config_action )
    config_menu.addAction( read_config_action )
    # Settings in the config file should override default values: this
    # is done reading the config file at startup (now)
    self.config.read_config()
    # Help menu
    help_menu = toolbar.addMenu('Help')
    self.help_window = None
    show_help_action = QtGui.QAction("Help",self)
    show_help_action.triggered.connect(self.activate_help_window)
    help_menu.addAction( show_help_action )
    
  def createLayout(self):
    main_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
    main_splitter.addWidget(self.mode_control)
    tab_widget = QtGui.QTabWidget()
    main_splitter.addWidget(tab_widget)

    psi_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
    psi_splitter.addWidget(self.psi_lims )
    psi_splitter.addWidget(self.psi_contr)
    psi_splitter.addWidget(self.psi_new_v)
    psi_splitter.setToolTip( """<html>
    <h3>Ψ control</h3>
     Ψ and ∇Ψ are avilable in the console:
     <ul>
      <li> use the <code>Psi</code> dictionary to get the functions
      <li> use the <code>setPsi</code> function to redefine them
     </ul>
     Use this panel to set:
     <ul>
      <li> the limits of the Ψ contours
      <li> the values of Ψ for the separatrix and the contours
      <li> the Ψ value of the selected and new vertexes
     </ul>
    </html>""" )

    mesh_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
    mesh_splitter.addWidget(self.lock_vertexes)
    mesh_splitter.addWidget(self.mu_sides     )
    mesh_splitter.addWidget(self.mesh_optimize)
    mesh_splitter.addWidget(self.mesh_refine  )

    editor_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
    editor_splitter.addWidget(self.vertex_editor)
    editor_splitter.addWidget(self.side_editor  )
    editor_splitter.setToolTip( """<html>
    <h3>Entity editor</h3>
     View and change the properties of the last selected entities.
    </html>""" )

    tab_widget.addTab(psi_splitter   ,"Ψ control")
    tab_widget.addTab(mesh_splitter  ,"Mesh"     )
    tab_widget.addTab(editor_splitter,"Entities" )

    centralLayout = QtGui.QHBoxLayout()
    centralLayout.addWidget(main_splitter)

    centralWidget = QtGui.QWidget()
    centralWidget.setLayout(centralLayout)
    self.setCentralWidget(centralWidget)

  def cons_pushVariables(self,variableDict):
    self.jpt_cons.pushVariables(variableDict)
  
  def cons_pullVariable(self,variable_name):
    return self.jpt_cons.pullVariable(variable_name)

  # Build the gui components -------------------------------------------

  def make_mode_control(self):
    button_group = QtGui.QButtonGroup()
    layout       = QtGui.QVBoxLayout()
    buttons      = {}
    for m in ['vertex','element','view']:
      b = QtGui.QRadioButton( m+" mode" )
      # When changin the mode two buttons are toggled: the one which
      # is selected, which calls the callback function, and the one
      # which is deselected, which registers itself as previous mode.
      def button_toggled(is_selected,m=m,b=b):
        if is_selected:
          self.change_gui_mode_callback(m)
        else:
          self.previous_mode = b
      b.toggled.connect( button_toggled )
      button_group.addButton(b)
      layout.addWidget(b)
      buttons[m] = b
    self.mode_control = QtGui.QGroupBox()
    self.mode_control.setLayout(layout)
    self.mode_control.group   = button_group
    self.mode_control.buttons = buttons

  def activate_help_window(self):
    """
    This is a method rather than a component because we need to open
    and close dynamically the help window.
    """
    if self.help_window is None:
      self.help_window = HelpBrowser(self.close_help_window,parent=self)
    self.help_window.show()
    self.help_window.activateWindow()

  def close_help_window(self):
    self.help_window.close()
    self.help_window = None

  # Self-callback methods ----------------------------------------------

  def react2_notified_active_toolbar_change(self,is_toolbar_mode):
    """
    When activating the toolbar we switch into "view" mode, afterwards
    we switch back to whatever mode we were in.

    Also, when entering "view" mode, the control is disabled.
    """
    if is_toolbar_mode:
      # Note: if the "view" mode is already set, resetting it does not
      # trigger any toggle event, so there is no overhead.
      self.mode_control.buttons["view"].setChecked(True)
      self.mode_control.setEnabled(False)
    else:
      self.previous_mode.setChecked(True)
      self.mode_control.setEnabled(True)

  # Other callback methods ---------------------------------------------

  def react2_notified_Psi_change(self,msg):
    # pass this to the contour handlers
    self.psi_contr.react2_notified_Psi_change(msg)
    self.psi_new_v.react2_notified_Psi_change(msg)

# ----------------------------------------------------------------------

def SetupViewer():
  global app
  app = QtGui.QApplication(sys.argv)

def StartViewer():
  global app
  sys.exit(app.exec_())

# ----------------------------------------------------------------------

if __name__ == '__main__':
  import numpy as np
  def cb1(xy,button):
    print("Callback 1: xy=",xy,", button=",button)
  def cb2(button,controller):
    print("Callback 2: button=",button,", controller=",controller)
  def scb(xy,direction,nsteps):
    print("Scroll callback: ",xy,direction,nsteps)
  def key_cb(key):
    print('Key pressed: "'+key+'"')
  def mode_cb(mode):
    print("New mode: "+mode)
  def cb_vdata(vdata):
    print("vdata : ",vdata)
  def cb_sdata(sdata):
    print("sdata : ",sdata)
  def cb_psi_set(psi):
    print("setting psi : ",psi)
  def cb_psi_unset():
    print("unsetting psi")
  def cb_lock():
    print("lock verts")
  def cb_unlock():
    print("unlock verts")
  def cb_mu(mu):
    print("set μ:",mu)
  def cb_unmu():
    print("unset μ")
  def cb_mesh_opt(omega,niter):
    print("optimizing mesh with ω = ",omega,", niter = ",niter)
    return 0.123e-4
  def cb_refine():
    print("refining mesh")
    return 10,20,30
  SetupViewer()
  mainwindow = MeshGUI()
  mainwindow.set_callbacks(cb1,cb2,scb,key_cb,mode_cb, \
    cb_psi_set,cb_psi_unset,cb_lock,cb_unlock,cb_mu,cb_unmu, \
    cb_mesh_opt,cb_refine,cb_vdata,cb_sdata)
  def setPsi(Psi,gradPsi):
    print("Setting Psi to",Psi,gradPsi)
  mainwindow.activate( { \
     "Ψ"  : (lambda x,y : np.sqrt((x-2.0)**2+y**2)) ,     \
     "∇Ψ" : (lambda x,y : 2.0*np.array([x-2.0,y])         \
                          / np.sqrt((x-2.0)**2+y**2)) } , \
     setPsi )
  mainwindow.show()
  StartViewer()

