<h1>Poloidal mesh designer</h1>

<h2>Defining &Psi;</h2>

<p>The potential &Psi; can be accessed and redefined in the <a
href="#console">console window</a> using the two fields
<b>Psi["&Psi;"]</b> and <b>Psi["&nabla;&Psi;"]</b> of the
corresponding dictionary and the <b>setPsi</b> function,
respectively. <i>Please, do not change directly <b>Psi</b> in the
console but use always <b>setPsi</b>, since this functions will
automatically update all the GUI entities.</i></p>

<p>The <b>&Psi; control</b> tab controls all the general settings
concerning the potential &Psi;.</p>

<p>The <b>&Psi; contour limits</b> panel defines a Cartesian grid
which will be used to draw the contours of &Psi; i.e. the magnetic
surfaces. This grid has no effect on the generated mesh and is only a
graphical support for the mesh design.</p>

<p>The <b>&Psi; contour editor</b> panel defines the &Psi; contours
corresponding to the separatrix and arbitrary additional contours.
These contours do not affect the generated mesh. <i>Currently, there
is no way to compute automatically the separatrix for a given &Psi;,
and what this panel does is simply using different colors for the
separatrix and the remaining contours.</i></p>

<p>The <b>&Psi; selected &amp; new vertexes</b> panel can be used to
move the selected vertexes to the chosen &Psi; contour, as well as
specifying that the new vertexes must be on that contour.</p>

<h3>Morphing</h3>

<p>It is possible to <em>morph</em> the &Psi; profile from the given
one to a new one, denoted &Psi;&prime;. This requires the following
steps:</p>
<ul>
<li> activate the <b>Morphing</b> control
<li> insert the name of the new profile &Psi;&prime;; this must be the
name of a variable defined in the <a href="#console">console</a>
corresponding to a dictionary with entries <tt><em>&lt;new
Psi&gt;</em>["&Psi;"]</tt> and <tt><em>&lt;new
Psi&gt;</em>["&nabla;&Psi;"]</tt>
<li> load &Psi;&prime;
<li> blend the two profiles with the slider
<li> deactivate the <b>Morphing</b> control.
</ul>
Notice that, when the control is deactivated, the new &Psi; profile is
substituted to the old one, which is then discarded.

<h2>Creating vertexes and elements</h2>

<p>In <b>vertex mode</b>, clicking on the mesh canvas creates new
vertexes. To select/deselect a vertex, right-click it. The properties
of the last selected vertex can be edited in the <b
id="vertex_editor">Vertex editor</b> panel of the <b>Entities</b> tab.
To delete all the selected vertexes press either <tt>"Backspace"</tt>
or <tt>"d"</tt>.</p>

<p>In <b>element mode</b>, clicking on the vertexes creates new
elements, together with their sides. To select/deselect a element or a
side, right-click it. The properties of the last selected side can be
edited in the <b id="side_editor">Side editor</b> panel of the
<b>Entities</b> tab. To delete all the selected entities (vertexes,
elements and sides) press either <tt>"Backspace"</tt> or
<tt>"d"</tt>.</p>

<p><i>Vertexes can be deleted only if there are no sides nor elements
connected to them.</i></p>

<p>To select/deselect all the entities use <tt>"A"</tt> and
<tt>"N"</tt>, respectively.</p>

<p>For vertexes, setting the <b>lock</b> flag implies that such
vertexes will not be moved on the corresponding surface, if they have
a &Psi; value, nor will be displaced during the <a
href="#mesh_optimization">grid optimization</a>.</p>

<p>For sides, setting the <b>&mu;</b> flag results in these sides
being included in the <b>e</b> array of the boundary edges when the
grid <a href="#export_mesh">is exported</a>.</p>

<h2>The view mode</h2>

<p>The <b>view mode</b> is entered automatically whenever the controls
of the mesh canvas menu are used, for instance to pan or zoom the
image.  Essentially, this mode deactivates clicking on the figure, so
that pan and zoom actions do not interfere with the grid entities.</p>

<h2>Global mesh operations</h2>

<p>The <b>Mesh</b> tab provides some controls to perform global grid
operations.</p>

<p>The <b>Lock selected vertexes</b> panel can be used to set the
<b>lock</b> flag of all the currently selected vertexes. To change
this flag for an individual vertex, use the <b><a
href="#vertex_editor">Vertex editor</a></b> panel.</p>

<p>The <b>Set &mu; selected sides</b> panel can be used to set the
<b>&mu;</b> flag of all the currently selected sides. To change this
flag for an individual side, use the <b><a href="#side_editor">Side
editor</a></b> panel.</p>

<p>The <b><a id="mesh_optimization">Optimize mesh</a></b> panel can be
used to make the mesh more uniform. Locked vertexes are not moved, and
vertexes with a &Psi; value remain on the prescribed surface. Since
the optimization is a nonlinear problem, one can selected an
underrelaxation coefficient &omega; as well as the number of
iterations.</p>

<p>The <b><a id="mesh_refinement">Refine mesh</a></b> panel can be
used to refine the mesh. During the mesh refinement, new vertexes
receive a &Psi; value and a <b>lock</b> flag if their parent vertexes
had one. Similarly, new sides are given a &mu; value if the parent
side had one.</p>

<h2>Mesh export/import</h2>

<p>In the console, the <b><a id="export_mesh">export_mesh</a></b>
function can be used to create three objects corresponding to the
vertexes, boundary sides and elements of the grid. These variables can
then be saved to disk from the console using standard python commands.</p>

<p>The dual function to <b><a href="#export_mesh">export_mesh</a></b>
is <b><a id="import_mesh">import_mesh</a></b>, which uses the same
format as the former and can be used to load a grid in the mesh
designer.</p>

<h2 id="console">Working from the console</h2>

<p>The console provides full access to the mesh designer. For instance,
the following command add a new vertex:
<tt>mesh_controller.mc.add_new_vertex( xy = [0.1,0.6] )</tt>.</p>

<p>To see a list of the references exported in the console from the
mesh designer, use <b>whos</b>.</p>

