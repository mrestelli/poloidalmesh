## Copyright (C) 2016  Marco Restelli
##
## This file is part of: Poloidal Mesh Designer
##
## Poloidal Mesh is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3 of the
## License, or (at your option) any later version.
##
## Poloidal Mesh is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with the Poloidal Mesh source; If not, see
## <http://www.gnu.org/licenses/>.
##
## author: Marco Restelli                   <marco.restelli@gmail.com>


from weakref import WeakMethod as wmet
import numpy as np
import scipy as sp
import scipy.sparse
import scipy.sparse.linalg

"""
Model of the poloidal grid


*** Psi ***

The functions for Ψ and  ∇Ψ are stored in a global variable Psi, so
that they can be accessed by any grid entity without carrying a
reference to them.

Psi should never be modified directly; instead, use the module
function setPsi. There is also a follower collection for entities
which must be notified about changes in the definition of the Psi
function; since the function is already available as a module variable
there is no need to use a state follower: it is enough being notified
whenever a chage takes place.


*** Controllers ***

It is useful to attach "controllers" to grid entities. These
controllers are created in the controller module, which uses them to
cache information into the grid entities. The ControlledGridEntity
class of the present module provides a mechanism to add and access
these controllers.

Notice that in this module controllers are opaque objects, which are
only stored and returned, but never used.


*** Followers and follower collections ***

Many grid entities accept followers for various properties. A follower
is a function with one argument, and it is called whenever the
corresponding property changes, as

   follower( new_property_value )

There are two types of followers:
* state followers are concerned with the state of the target entity,
  and they are notified 
  a) when they are registered, using the state at the time of the
     registration
  b) whenever the state changes
* action followers are concerned with transformations occurring in the
  target object, and they are notified whenever such transformation
  occurs.

This means that state followers can be registered only once the target
entity has acquired some meaningful state, and they are immediately
notified about such state. Action followers, on the contrary, can be
registered for any entity, and they do not get any notification until
the corresponding transformation occurs.

In particular, a state follower is required for those attributes which
are set directly by the constructor and never change during the object
life.

For the implementation details of the follower mechanism, see the
FollowerCollection class.

"""

# ----------------------------------------------------------------------

class FollowerCollection():
  """
  Collects and manipulates a collection of followers.

  Followers are stored through WeakMethod references: this guarantees
  that holding a reference to the follower does not interfere with the
  life cycle of the object to which the follower method belongs.
  """

  __slots__ = '_followers'

  def __init__(self):
    self._followers = set()

  def notify_followers(self,message):
    """
    This function takes care of the fact that follower methods are
    stored in WeakMethod references; in particular, live followers are
    notified, while dead ones are removed.
  
    message: whatever data must be sent to the followers
    """
    dead_followers = set()
    for f_wrap in self._followers:
      f = f_wrap() # unwrap the follower
      if f is None:
        dead_followers.add(f_wrap)
      else:
        f(message)
    for df in dead_followers:
      self._followers.remove(df)

  def rm_follower(self,follower_to_be_removed):
    self._followers.remove( wmet(follower_to_be_removed) )


class StateFollowerCollection(FollowerCollection):
  """
  State followers are concerned with the state of the target entity,
  and they are notified 
  a) when they are registered, using the state at the time of the
     registration
  b) whenever the state changes

  See also the module documentation.
  """

  __slots__ = []

  def add_follower(self,new_follower,state):
    """
    Subscribe a new follower

    new_follower: follower function
    state:        present state
    """
    self._followers.add(wmet(new_follower))
    new_follower(state)


class ActionFollowerCollection(FollowerCollection):
  """
  Action followers are concerned with transformations occurring in the
  target object, and they are notified whenever such transformation
  occurs.

  See also the module documentation.
  """

  __slots__ = []

  def add_follower(self,new_follower):
    """
    Subscribe a new follower
  
    new_follower: follower function
    """
    self._followers.add(wmet(new_follower))

# ----------------------------------------------------------------------

Psi = { "Ψ" : None , "∇Ψ" : None }

_followers_Psi_change = ActionFollowerCollection()

def setPsi( Ψ=None , gradΨ=None ):
  """
  Setter for the global variable Psi

  Ψ:     x,y -> Ψ(x,y)   (scalar)
  gradΨ: x,y -> [ ∂Ψ/∂x , ∂Ψ/∂y ]

  """
  global Psi
  Psi["Ψ"]  = Ψ
  Psi["∇Ψ"] = gradΨ
  _followers_Psi_change.notify_followers( None )

def add_follower_Psi_change(follower):
  _followers_Psi_change.add_follower(follower)

def rm_follower_Psi_change(follower):
  _followers_Psi_change.rm_follower(follower)

# ----------------------------------------------------------------------

class ControlledGridEntity():
  """
  Provides some methods to attach a controller.

  Besides handling the controllers, this class defines some slots
  which are common to all grid entity classes:
  __weakref__ : is used for entities which have to be followed by
                other grid entities, such as vertexes followed by
                sides
            i : allows attaching a number to a grid entity
  """

  __slots__ = '__weakref__', 'i', '_controller'

  def __init__(self):
    self._controller = None

  @property
  def controller(self):
    return self._controller

  def set_controller(self,controller):
    self._controller = controller

# ----------------------------------------------------------------------

class Vertex(ControlledGridEntity):
  """
  Accepts followers for changes in:
  - locked
  - psi
  - xy
  """

  __slots__ = '_lock', '_psi', '_xy', '_s', '_e', \
    '_followers_lock_change', '_followers_psi_change', \
    '_followers_xy_change'

  def __init__(self):
    super().__init__()
    self._lock = False
    self._psi  = None
    self._xy   = np.array([np.nan,np.nan],dtype=float)
    # connectivity
    self._s   = set() # connected sides
    self._e   = set() # connected el. (strong ref: don't rely on GC)
    # prepare the list of the notification methods
    self._followers_lock_change = StateFollowerCollection()
    self._followers_psi_change  = StateFollowerCollection()
    self._followers_xy_change   = StateFollowerCollection()

  def add_follower_lock_change(self,follower):
    self._followers_lock_change.add_follower(follower,self.lock)

  def add_follower_psi_change(self,follower):
    self._followers_psi_change.add_follower(follower,self.psi)

  def add_follower_xy_change(self,follower):
    self._followers_xy_change.add_follower(follower,self.xy)

  @property
  def ns(self):
    return len(self.s)

  @property
  def ne(self):
    return len(self.e)

  @property
  def s(self):
    return self._s

  @property
  def e(self):
    return self._e

  @property
  def lock(self):
    return self._lock

  @property
  def psi(self):
    return self._psi

  @property
  def xy(self):
    return self._xy

  def set_lock(self,lock):
    if self._lock != lock: # avoid useless state changes
      self._lock = lock
      if not self.lock:
        self.set_xy(self.xy) # in case psi is set
      self._followers_lock_change.notify_followers( self.lock )

  def set_psi(self,psi):
    if self._psi != psi: # avoid useless state changes
      # subscribe to Psi when going from None->psi and vice versa
      #
      # Note: followers are collected in sets, so there is no problem
      # adding the same follower multiple times. However, it is more
      # efficient to avoid this.
      if (self._psi is None) and (psi is not None):
        add_follower_Psi_change(self.react2_notified_Psi_change)
      if (self._psi is not None) and (psi is None):
        rm_follower_Psi_change(self.react2_notified_Psi_change)
      # update psi
      self._psi = psi
      if not np.isnan(self.xy[0]): self.set_xy(self.xy)
      self._followers_psi_change.notify_followers( self.psi )

  def _set_xy_silent(self,xy):
    if (not self.lock) and (self.psi is not None):
      xy = self.adjust_xy_to_psi_surface(self.psi,xy)
    self._xy[:] = xy

  def set_xy(self,xy):
    if len(xy)!=2: raise ValueError('Vertexes must have two coords')
    xy_old = [ xi for xi in self.xy ]
    self._set_xy_silent(xy)
    if any( self.xy != xy_old ): # avoid useless notifications
      self._followers_xy_change.notify_followers( self.xy )

  def add_side(self,s):
    self._s.add(s)

  def add_element(self,e):
    self._e.add(e)

  def remove_side(self,s):
    self._s.remove(s)

  def remove_element(self,e):
    self._e.remove(e)

  def react2_notified_Psi_change(self,msg):
    # The following call takes into account the new Psi, the lock and
    # any required notification if the coordinates change.
    self.set_xy(self.xy)

  @staticmethod
  def adjust_xy_to_psi_surface(psi,xy):
    """
    Iterations based on the following construction:
    1) set   Ψl(x,y) = Ψ0 + ∂Ψ/∂x * (x-x0) + ∂Ψ/∂y * (y-y0) 
    2) cosider the line  xy(s) = xy0 + gradΨ0 * s  (s scalar)
    3) determine  s  such that  Ψl( x(s) , y(s) ) = psi

    Note: assumes that the Psi dictionary contains callable functions
    """
    s = 1.0e9
    xy_new = np.array(xy)
    k = 0
    Ψ , gradΨ = Psi["Ψ"] , Psi["∇Ψ"]
    while( (abs(s)>1.0e-14) and (k<100) ):
      k += 1
      Ψ0     =              Ψ(*xy_new)
      gradΨ0 = np.array(gradΨ(*xy_new))
      s = (psi - Ψ0)/np.sum(gradΨ0**2)
      xy_new_tmp = xy_new + gradΨ0*s
      while(abs(Ψ(*xy_new_tmp))>1.1*abs(Ψ0)):
        s = 0.5*s
        xy_new_tmp = xy_new + gradΨ0*s
      xy_new = xy_new_tmp
    return xy_new
  
# ----------------------------------------------------------------------

class Side(ControlledGridEntity):
  """
  Accepts followers for changes in:
  - mu (boundary marker)
  - xy
  """

  __slots__ = '_mu', '_v', '_e', 'mp', \
    '_followers_mu_change', '_followers_xy_change'

  def __init__(self,vs):
    super().__init__()
    self._mu = None
    # define the side invariants: vertexes
    self._v = set(vs)
    if len(self.v)!=2: raise ValueError( \
      'Creating a side requires exactly two distinct vertexes')
    # connectivity
    self._e = set()
    # prepare the list of the notification methods
    self._followers_mu_change = StateFollowerCollection()
    self._followers_xy_change = StateFollowerCollection()

  def add_follower_mu_change(self,follower):
    self._followers_mu_change.add_follower(follower,self.mu)

  def add_follower_xy_change(self,follower):
    self._followers_xy_change.add_follower(follower,self.xy)

  @property
  def nv(self):
    return len(self.v)

  @property
  def ne(self):
    return len(self.e)

  @property
  def v(self):
    return self._v

  @property
  def e(self):
    return self._e

  @property
  def key(self):
    """
    Returns a key identifying the logical side, given the vertexes
    """
    return tuple(sorted( [v for v in self.v] , key=lambda v: id(v) ))

  @property
  def mu(self):
    return self._mu

  @property
  def xy(self):
    return [v.xy for v in self.v]

  def add_element(self,e):
    self._e.add(e)

  def remove_element(self,e):
    self._e.remove(e)

  def set_mu(self,mu):
    if self._mu != mu: # avoid useless state changes
      msg = 'μ must be a positive integer'
      if mu is None:          raise ValueError( msg )
      if type(mu) is not int: raise ValueError( msg )
      if mu <= 0:             raise ValueError( msg )
      self._mu = mu
      self._followers_mu_change.notify_followers( self.mu )

  def react2_notified_vertxy_change(self,xy):
    # Changing the v coords changes also the side coords.
    self._followers_xy_change.notify_followers( self.xy )

# ----------------------------------------------------------------------

class Element(ControlledGridEntity):
  """
  Accepts notification callbacks for changes in:
  - xy change
  """

  __slots__ = '_v', '_s', \
    '_followers_xy_change'

  def __init__(self,vs):
    super().__init__()
    # define the element invariants: vertexes
    self._v = [] # make sure we have a list without repetitions
    for v in vs:
      if v in self.v: raise ValueError('Vertexes can not be repeated')
      self._v.append(v)
    if len(self.v)<3: raise ValueError( \
      'Can not create an element with only '+str(len(self.v))+' verts.')
    # connectivity
    self._s = []
    # prepare the list of the notification methods
    self._followers_xy_change = StateFollowerCollection()

  def add_follower_xy_change(self,follower):
    self._followers_xy_change.add_follower(follower,self.xy)

  @property
  def poly(self):
    return len(self.v)

  @property
  def v(self):
    return self._v

  @property
  def s(self):
    return self._s

  @property
  def xy(self):
    return [v.xy for v in self.v]

  def add_side(self,s):
    self._s.append(s)

  def react2_notified_vertxy_change(self,xy):
    # Changing the v coords changes also the element coords.
    self._followers_xy_change.notify_followers( self.xy )

# ----------------------------------------------------------------------

class MeshModel():
  """
  Note: in the present implementation, side creation and destruction
  are subordinated to element creation and destruction.

  Accepts notification callbacks for changes in:
  - new_vert_psi
  - add_new_v
  - add_new_s
  - add_new_e
  - remove_v
  - remove_s
  - remove_e
  """

  def __init__(self):
    # main fields
    self._new_vert_psi = None # psi value used for new vertexes
    self._v = set()
    self._s = {}    # a dictionary simplifies checking for duplicates
    self._e = set()
    # prepare the list of the notification methods
    self._followers_new_vert_psi_change = StateFollowerCollection()
    self._followers_add_new_v_change    = ActionFollowerCollection()
    self._followers_add_new_s_change    = ActionFollowerCollection()
    self._followers_add_new_e_change    = ActionFollowerCollection()
    self._followers_remove_v_change     = ActionFollowerCollection()
    self._followers_remove_s_change     = ActionFollowerCollection()
    self._followers_remove_e_change     = ActionFollowerCollection()
  
  def add_follower_new_vert_psi_change(self,follower):
    self._followers_new_vert_psi_change.add_follower( \
                         follower , self.new_vert_psi )
    
  def add_follower_add_new_v_change(self,follower):
    self._followers_add_new_v_change.add_follower( follower )

  def add_follower_add_new_s_change(self,follower):
    self._followers_add_new_s_change.add_follower( follower )

  def add_follower_add_new_e_change(self,follower):
    self._followers_add_new_e_change.add_follower( follower )
    
  def add_follower_remove_v_change(self,follower):
    self._followers_remove_v_change.add_follower( follower )

  def add_follower_remove_s_change(self,follower):
    self._followers_remove_s_change.add_follower( follower )

  def add_follower_remove_e_change(self,follower):
    self._followers_remove_e_change.add_follower( follower )

  @property
  def new_vert_psi(self):
    return self._new_vert_psi

  @property
  def v(self):
    return self._v
  @property
  def s(self):
    return self._s.values()
  @property
  def e(self):
    return self._e

  @property
  def nv(self):
    return len(self.v)
  @property
  def ns(self):
    return len(self.s)
  @property
  def ne(self):
    return len(self.e)

  def set_new_vert_psi(self,new_vert_psi):
    self._new_vert_psi = new_vert_psi
    self._followers_new_vert_psi_change.notify_followers( \
                                        self.new_vert_psi )

  def add_new_vertex(self,xy):
    v = Vertex()
    v.set_psi(self.new_vert_psi)
    v.set_xy(xy)
    self._v.add(v)
    self._followers_add_new_v_change.notify_followers( v )
    return v
    
  def remove_vertex(self,v):
    remove_is_allowed = (v.ns==0) and (v.ne==0)
    if remove_is_allowed:
      self._v.remove(v)
      self._followers_remove_v_change.notify_followers( v )
    
  def add_new_side(self,vs):
    # generate the side and store it, if it is new
    if any( [v not in self.v for v in vs] ): raise ValueError( \
      'To build a side, all the vertexes must be in the grid')
    s = Side(vs)
    try:
      s = self._s[s.key] # known side
    except KeyError:
      self._s[s.key] = s # new side
      # update the mesh connectivity: vertexes
      for v in s.v:
        v.add_side(s)
        v.add_follower_xy_change(s.react2_notified_vertxy_change)
      self._followers_add_new_s_change.notify_followers( s )
    return s
    
  def remove_side(self,s):
    remove_is_allowed = (s.ne==0)
    if remove_is_allowed:
      for v in s.v: v.remove_side(s) # vertexes must be notified
      del self._s[s.key]
      self._followers_remove_s_change.notify_followers( s )
    
  def add_new_element(self,vs):
    # generate the element and store it
    e = Element(vs)
    self._e.add(e)
    # update the mesh connectivity: vertexes and sides
    for v in e.v:
      v.add_element(e)
      v.add_follower_xy_change(e.react2_notified_vertxy_change)
    for svs in zip( e.v , np.roll(e.v,-1) ):
      s = self.add_new_side(svs)
      # side-element connectivity update: same for old and new sides
      e.add_side(s)
      s.add_element(e)
    # notify the followers
    self._followers_add_new_e_change.notify_followers( e )
    # done
    return e
    
  def remove_element(self,e):
    for v in e.v: v.remove_element(e) # vertexes must be notified
    for s in e.s: s.remove_element(e) # sides must be notified
    self._e.remove(e)
    self._followers_remove_e_change.notify_followers( e )
    # try removing the sides
    for s in e.s: self.remove_side(s)
    
  def export_mesh(self):
    # Vertexes
    p = np.ndarray((4,len(self.v)),dtype=float)
    for i,v in enumerate(self.v):
      # assign an index to each vertex
      v.i = i
      # export the vertex coordinates
      p[0:2,i] = v.xy
      # export psi
      p[2,i] = np.nan if v.psi is None else v.psi
      # mark locked vertexes
      p[3,i] = 1 if v.lock else 0
    # Elements
    t = []
    for i,e in enumerate(self.e):
      nv = len(e.v)
      for j in range(len(t),nv-2):
        # for efficiency, we start with list and then make np.array
        t.append( { 'ie':[] , 'it':[] } )
      t[nv-2-1]['ie'].append(i)
      t[nv-2-1]['it'].append( [ v.i for v in e.v ] )
    t = [ tt for tt in t if tt['ie'] ] # drop empty lists
    for tt in t:
      tt['ie'] = np.array( tt['ie'] ,dtype=int) + 1 # 1-based indexes
      tt['it'] = np.array( tt['it'] ,dtype=int).T + 1
    # Boundary sides
    nse = 0 # number of sides in e
    e = np.empty( (2+2+1,self.ns) , dtype=int )
    for s in self.s:
      if s.mu is not None:
        e[:,nse] = [v.i+1 for v in s.v] + [0,0] + [s.mu] # 1-based idx
        nse += 1
    return p , e[:,:nse] , t

  def import_mesh(self,p,e,t):
    """
    Add vertexes and elements to the mesh - same format as export_mesh
    """
    self.set_new_vert_psi(None)
    tmp_vlist = [] # a list is needed to interpret t
    for vdata in p.T:
      xy,psi,lock = vdata[0:2],vdata[2],vdata[3]
      v = self.add_new_vertex(xy)
      v.set_lock( lock==1 )
      v.set_psi( None if np.isnan(psi) else psi )
      tmp_vlist.append(v)
    for tt in t:
      for iv in tt['it'].T:
        self.add_new_element( [tmp_vlist[i-1] for i in iv] )
    for ei in e.T:
      s = self._s[Side( [tmp_vlist[i-1] for i in ei[0:2]] ).key]
      s.set_mu(int(ei[4]))

  def refine_mesh(self):
    """
    Refine the mesh

    0) copy the lists of sides and elements
    1) for each side, add a vertex in the midpoint
    2) for each element:
      2.1) generate the subelements
      2.2) delete the element (and also the sides)
    """
    old_s = [s for s in self.s]
    old_e = self.e.copy()
    # generate the new vertexes
    self.set_new_vert_psi(None)
    for s in old_s:
      vs = s.v
      s.mp = self.add_new_vertex(np.mean([v.xy for v in vs],axis=0))
      if all(v.lock for v in vs):
        s.mp.set_lock(True)
      if all(v.psi is not None for v in vs):
        s.mp.set_psi(np.mean([v.psi for v in vs]))
    for e in old_e:
      if e.poly==3:
        e0 = self.add_new_element([e.s[0].mp,e.s[1].mp,e.s[2].mp])
        e1 = self.add_new_element([e.s[0].mp, e.v[1]  ,e.s[1].mp])
        e2 = self.add_new_element([e.s[1].mp, e.v[2]  ,e.s[2].mp])
        e3 = self.add_new_element([e.s[2].mp, e.v[0]  ,e.s[0].mp])
        # propagate the boundary markers
        e1.s[0].set_mu( e.s[0].mu ); e1.s[1].set_mu( e.s[1].mu )
        e2.s[0].set_mu( e.s[1].mu ); e2.s[1].set_mu( e.s[2].mu )
        e3.s[0].set_mu( e.s[2].mu ); e3.s[1].set_mu( e.s[0].mu )
      else:
        # add the barycenter
        vs = e.v
        c = self.add_new_vertex(np.mean([v.xy for v in vs],axis=0))
        if all(v.psi is not None for v in vs):
          c.set_psi(np.mean([v.psi for v in vs]))
        for i in range(e.poly):
          ip = (i+1) % e.poly
          ei = self.add_new_element([e.s[i].mp,c,e.s[ip].mp,e.v[ip]])
          # propagate the boundary markers
          ei.s[2].set_mu( e.s[ip].mu ); ei.s[3].set_mu( e.s[i].mu )

      self.remove_element(e) # also removes the sides

  def optimize_mesh(self,omega,n_iter):
    """
    Regularize the mesh

    Doing more iterations at once is more efficient, since it allows a
    single notification to the followers.
    """
    gradPsi = Psi["∇Ψ"]
    for i_iter in range(n_iter):
      # number the vertexes to define the linear system
      for i,v in enumerate(self.v):
        v.i = i # assign an index to each vertex
      # compute the local stiffness matrices
      nv = len(self.v)
      ns = len(self.s)
      ii = np.empty( (2*4*ns,) , dtype=int   )
      jj = np.empty( (2*4*ns,) , dtype=int   )
      xx = np.empty( (2*4*ns,) , dtype=float )
      pos = -1
      k_loc = np.array([[-1.0,1.0],[1.0,-1.0]])
      for s in self.s:
        for i,v1 in enumerate(s.v):
          for j,v2 in enumerate(s.v):
            # x component
            pos += 1
            ii[pos] = 2*v1.i
            jj[pos] = 2*v2.i
            xx[pos] = k_loc[i,j]
            # y component
            pos += 1
            ii[pos] = 2*v1.i+1
            jj[pos] = 2*v2.i+1
            xx[pos] = k_loc[i,j]
      # build the linear system (linked list format)
      aa = sp.sparse.coo_matrix( (xx , (ii,jj)) , (2*nv,2*nv) ).tolil()
      bb = np.zeros((2*nv,))
      # enforce the constrains
      rot = np.array([[0,-1],[1,0]])
      for v in self.v:
        if v.lock or (v.ne==0): # verts not connected treated as locked
          ix = 2*v.i; iy = 2*v.i+1
          aa[ix,:] = 0; aa[ix,ix] = 1.0; bb[ix] = v.xy[0]
          aa[iy,:] = 0; aa[iy,iy] = 1.0; bb[iy] = v.xy[1]
        else:
          if v.psi is not None:
            gradPsiv = np.array(gradPsi(*v.xy))
            nn = gradPsiv/np.sqrt(np.sum(gradPsiv**2))
            tt = np.dot(rot,nn)
            ix = 2*v.i; iy = 2*v.i+1
            # tangential equation
            aa[ix,:] = tt[0]*aa[ix,:] + tt[1]*aa[iy,:]
            # normal equation:   (x-x0)*n = 0
            aa[iy,:] = 0; aa[iy,ix] = nn[0]; aa[iy,iy] = nn[1]; 
            bb[iy] = np.dot(nn,v.xy)
      # solve the linear system and compute dxy
      xy = scipy.sparse.linalg.spsolve(aa.tocsc(),bb)
      dxy = xy
      for v in self.v:
        ix = 2*v.i; iy = 2*v.i+1
        dxy[ix:iy+1] -= v.xy
      # move the points
      if i_iter==n_iter-1: # last iteration: update the followers
        max_dxy = 0
        for v in self.v:
          ix = 2*v.i; iy = 2*v.i+1
          v.set_xy( v.xy + omega*dxy[ix:iy+1] )
          max_dxy = max(max_dxy,np.sqrt(np.sum(dxy**2)))
      else: # no need to update the followers
        for v in self.v:
          ix = 2*v.i; iy = 2*v.i+1
          v._set_xy_silent( v.xy + omega*dxy[ix:iy+1] )

    return max_dxy, aa, bb, xy

