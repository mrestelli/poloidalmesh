## Copyright (C) 2016  Marco Restelli
##
## This file is part of: Poloidal Mesh Designer
##
## Poloidal Mesh is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3 of the
## License, or (at your option) any later version.
##
## Poloidal Mesh is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with the Poloidal Mesh source; If not, see
## <http://www.gnu.org/licenses/>.
##
## author: Marco Restelli                   <marco.restelli@gmail.com>


from abc import \
  ABCMeta, abstractmethod
from mesh_model import \
  Psi    as mesh_model_Psi,    \
  setPsi as mesh_model_setPsi, \
  add_follower_Psi_change,     \
  MeshModel, \
  ActionFollowerCollection, StateFollowerCollection
from mesh_viewer_mpl_backend import \
  SuspendRefresh
from mesh_viewer_qt import \
  MeshGUI, SetupViewer, StartViewer


# ----------------------------------------------------------------------

y0 = 1.5
psi_sep = y0**2/6.0

def Psi_default(x,y):
  import numpy as np
  return 0.5*((x-2)**2+y**2) + y**3/(3.0*y0)

def gradPsi_default(x,y):
  import numpy as np
  return np.array([ x-2 , y + y**2/y0 ])

# ----------------------------------------------------------------------

class EntityList():
  """
  A simple class to manage an entity list.

  Some features:
  * no repetitions
  * duplicated entries are inserted only once
  * it is safe to destroy a listed entity (notifying the list)

  Accepts notification callbacks for changes in:
  - list insertion
  - list removal
  - last entry change
  """

  __slots__ = '_data', '_followers_entry_inserted', \
    '_followers_entry_removed', '_followers_last_entry_change', \
    '__weakref__'

  def __init__(self):
    self._data = []
    # prepare the list of the notification methods
    self._followers_entry_inserted    = ActionFollowerCollection()
    self._followers_entry_removed     = ActionFollowerCollection()
    self._followers_last_entry_change = StateFollowerCollection()

  def __iter__(self):
    return iter(self._data)

  def __getitem__(self,i):
    return self._data[i]

  def add_follower_entry_inserted(self,follower):
    self._followers_entry_inserted.add_follower(follower)

  def add_follower_entry_removed(self,follower):
    self._followers_entry_removed.add_follower(follower)

  def add_follower_last_entry_change(self,follower):
    self._followers_last_entry_change.add_follower(follower,self.last)

  @property
  def size(self):
    return len(self._data)

  @property
  def last(self):
    return self._data[-1] if self.size>0 else None

  def add_to_list(self,x):
    if x not in self._data:
      self._data.append(x)
      self._followers_entry_inserted.notify_followers( x )
      self._followers_last_entry_change.notify_followers( self.last )
      
  def remove_from_list(self,x):
    if x in self._data:
      removing_last = x is self.last
      self._data.remove(x)
      self._followers_entry_removed.notify_followers( x )
      if removing_last:
        self._followers_last_entry_change.notify_followers( self.last )

  def clear_list(self):
    if self.size > 0:
      for x in self._data:
        self._followers_entry_removed.notify_followers( x )
      self._data = []
      self._followers_last_entry_change.notify_followers( self.last )

  def react2_notified_entry_destroyed(self,x):
    self.remove_from_list(x)


class SelectableEntity():
  """
  Controller for an entity supporting selection

  Accepts notification callbacks for:
  - entity selection
  """

  __slots__ = '_selected', '_followers_selected_change'

  def __init__(self):
    self._selected = False
    self._followers_selected_change = StateFollowerCollection()

  def add_follower_selected_change(self,follower):
    self._followers_selected_change.add_follower(follower,self.selected)

  @property
  def selected(self):
    return self._selected

  def select(self):
    if not self.selected:
      self._selected = True
      self._followers_selected_change.notify_followers( self.selected )

  def deselect(self):
    if self.selected:
      self._selected = False
      self._followers_selected_change.notify_followers( self.selected )

# ----------------------------------------------------------------------

class Vertex(SelectableEntity):
  """
  Vertex controller

  Accepts notification callbacks for:
  - vertex selection (from the base class)
  """

  __slots__ = 'mv', 'vv'

  def __init__(self,mv,vv):
    super().__init__()
    self.mv = mv # model vertex
    self.vv = vv # viewer vertex

  def set_lock(self,lock):
    self.mv.set_lock(lock)

  def set_psi(self,psi):
    self.mv.set_psi(psi)


class Side(SelectableEntity):
  """
  Side controller

  Accepts notification callbacks for:
  - vertex selection (from the base class)
  """

  __slots__ = 'ms', 'vs'

  def __init__(self,ms,vs):
    super().__init__()
    self.ms = ms # model side
    self.vs = vs # viewer side

  def set_mu(self,mu):
    self.ms.set_mu(mu)


class Element(SelectableEntity):
  """
  Element controller

  Accepts notification callbacks for:
  - element selection (from the base class)
  """

  __slots__ = 'me', 've'

  def __init__(self,me,ve):
    super().__init__()
    self.me = me # model vertex
    self.ve = ve # viewer vertex

# ----------------------------------------------------------------------

EntityTypes = ( Vertex , Side , Element )

# ----------------------------------------------------------------------

class Mesh():
  """
  Mesh controller

  Accepts notification callbacks for:
  - vertex removal
  - side removal
  - element removal
  """

  def __init__( self , mesh_mod,mesh_gui_bkend ):
    mm = mesh_mod
    vm = mesh_gui_bkend

    mm.add_follower_add_new_v_change( \
                          self.react2_notified_add_new_v_change)
    mm.add_follower_add_new_s_change( \
                          self.react2_notified_add_new_s_change)
    mm.add_follower_add_new_e_change( \
                          self.react2_notified_add_new_e_change)
    mm.add_follower_remove_v_change( \
                           self.react2_notified_remove_v_change)
    mm.add_follower_remove_s_change( \
                           self.react2_notified_remove_s_change)
    mm.add_follower_remove_e_change( \
                           self.react2_notified_remove_e_change)
    # set the fields
    self.mm = mm # model mesh
    self.vm = vm # viewer mesh
    self.v = set()
    self.s = set()
    self.e = set()
    # prepare the list of the notification methods: entity removal
    self._followers_vsl_removed = { \
      key: ActionFollowerCollection() for key in EntityTypes }

  def add_follower_vsl_removed(self,entity_type,follower):
    self._followers_vsl_removed[entity_type].add_follower( follower )

  def add_new_vertex(self,xy):
    # The vertex is added to the model mesh; the viewer and controller
    # vertexes are generated in the callback of this class.
    self.mm.add_new_vertex(xy)
    
  def remove_vertex(self,v):
    self.mm.remove_vertex(v.mv)

  def add_new_element(self,vs):
    # Same as for vertexes
    self.mm.add_new_element([v.mv for v in vs])

  def remove_element(self,e):
    self.mm.remove_element(e.me)

  def set_psi_new_vertexes(self,psi):
    self.mm.set_new_vert_psi(psi)
  
  def react2_notified_add_new_v_change(self,mv):
    vv = self.vm.add_new_vertex() # add to viewer mesh
    v = Vertex(mv,vv)
    # setup all the notification callbacks
    mv.add_follower_xy_change(     vv.react2_notified_xy_change      )
    mv.add_follower_psi_change(    vv.react2_notified_psi_change     )
    mv.add_follower_lock_change(   vv.react2_notified_lock_change    )
    v.add_follower_selected_change(vv.react2_notified_selected_change)
    # set the controllers
    mv.set_controller(v)
    vv.set_controller(v)
    # update the controller mesh
    self.v.add(v)                 # add to controller mesh

  def react2_notified_add_new_s_change(self,ms):
    vs = self.vm.add_new_side() # add to viewer mesh
    s = Side(ms,vs)
    # setup all the notification callbacks
    ms.add_follower_mu_change(     vs.react2_notified_mu_change      )
    ms.add_follower_xy_change(     vs.react2_notified_xy_change      )
    s.add_follower_selected_change(vs.react2_notified_selected_change)
    # set the controllers
    ms.set_controller(s)
    vs.set_controller(s)
    # update the controller mesh
    self.s.add(s)                 # add to controller mesh

  def react2_notified_add_new_e_change(self,me):
    ve = self.vm.add_new_element() # add to viewer mesh
    e = Element(me,ve)
    # setup all the notification callbacks
    me.add_follower_xy_change(     ve.react2_notified_xy_change      )
    e.add_follower_selected_change(ve.react2_notified_selected_change)
    # set the controller
    me.set_controller(e)
    ve.set_controller(e)
    # update the controller mesh
    self.e.add(e)                  # add to controller mesh

  def react2_notified_remove_v_change(self,mv):
    v = mv.controller
    self.vm.remove_vertex(v.vv) # remove from viewer mesh
    self.v.remove(v)            # remove from controller mesh
    self._followers_vsl_removed[Vertex].notify_followers( v )

  def react2_notified_remove_s_change(self,ms):
    s = ms.controller
    self.vm.remove_side(s.vs) # remove from viewer mesh
    self.s.remove(s)          # remove from controller mesh
    self._followers_vsl_removed[Side].notify_followers( s )

  def react2_notified_remove_e_change(self,me):
    e = me.controller
    self.vm.remove_element(e.ve) # remove from viewer mesh
    self.e.remove(e)             # remove from controller mesh
    self._followers_vsl_removed[Element].notify_followers( e )

# ----------------------------------------------------------------------

class Mode(metaclass=ABCMeta):
    
  __slots__ = '__weakref__', 'mc', '_selection_lists'

  def __init__(self,mc):
    """
    Stores a reference to the mesh controller

    Includes also selection lists for the grid entities.
    """
    self.mc = mc
    self._selection_lists = { key: EntityList() for key in EntityTypes }
    # We need to be notified if an entity is removed from the grid
    for k,l in self._selection_lists.items():
      self.mc.add_follower_vsl_removed( k , \
        self.react2_notified_entry_destroyed )
    # Also, if an entry is added or removed it must be selected/desel.
    for k,l in self._selection_lists.items():
      l.add_follower_entry_inserted( \
         self.react2_notified_entry_selected)
      l.add_follower_entry_removed( \
         self.react2_notified_entry_deselected)

  def add_to_selection_list(self,entity):
    entity.select()
    self._selection_lists[type(entity)].add_to_list(entity)

  def remove_from_selection_list(self,entity):
    entity.deselect()
    self._selection_lists[type(entity)].remove_from_list(entity)

  @abstractmethod
  def mouse_click(self,xy,button): pass

  @abstractmethod
  def object_pick(self,button,controller): pass

  @abstractmethod
  def key_press(self,key): pass

  @abstractmethod
  def enter_mode(self,old_mode):
    """
    Switch from old_mode to the new mode.
    """
    pass

  def get_selected(self,entity_type):
    return self._selection_lists[entity_type]

  def get_last_in_selection_list(self,entity_type):
    return self._selection_lists[entity_type].last

  def add_follower_last_sel_entity_change(self,entity_type,follower):
    # Change in the last selected vertex
    self._selection_lists[ entity_type \
      ].add_follower_last_entry_change( follower )

  def react2_notified_entry_destroyed(self,entity):
    self.remove_from_selection_list(entity)

  def react2_notified_entry_selected(self,entity):
    entity.select()

  def react2_notified_entry_deselected(self,entity):
    entity.deselect()


class VertexMode(Mode):
  
  __slots__ = ()

  def __init__(self,mc):
    super().__init__(mc)

  def mouse_click(self,xy,button):
    if button==1: # add a vertex
      self.mc.add_new_vertex(xy)

  def object_pick(self,button,controller):
    if isinstance(controller,Vertex):
      if button==3: # select/deselect the vertex
        if controller.selected:
          self.remove_from_selection_list(controller)
        else:
          self.add_to_selection_list(controller)

  def key_press(self,key):
    if key=="A": # select all verts
      with SuspendRefresh():
        for v in self.mc.v:
          self.add_to_selection_list(v)
    elif key=="N": # deselect all verts
      with SuspendRefresh():
        for l in self._selection_lists.values():
          l.clear_list()

  def enter_mode(self,old_mode):
    # at start-up, old_mode can be None
    old_selection_lists = old_mode._selection_lists \
      if old_mode is not None else {}
    # old_mode might use the same lists this mode
    if old_selection_lists is not self._selection_lists:
      with SuspendRefresh():
        for l in old_selection_lists.values():
          for entity in l:
            entity.deselect()
        for v in self.get_selected(Vertex):
          v.select() 


class ElementMode(Mode):
  
  __slots__ = ()

  def __init__(self,mc):
    super().__init__(mc)

  def mouse_click(self,xy,button):
    pass

  def object_pick(self,button,controller):
    if isinstance(controller,Vertex):
      if button==1 or button==3: # select vert., possibly generate el.
        self._new_vertex_for_the_el_builder(controller)
    if isinstance(controller,Side):
      if button==3: # select/deselect side
        if controller.selected:
          self.remove_from_selection_list(controller)
        else:
          self.add_to_selection_list(controller)
    if isinstance(controller,Element):
      if button==3: # select/deselect element
        if controller.selected:
          self.remove_from_selection_list(controller)
        else:
          self.add_to_selection_list(controller)

  def key_press(self,key):
    if key=="A": # select all elements
      with SuspendRefresh():
        for s in self.mc.s:
          self.add_to_selection_list(s)
        for e in self.mc.e:
          self.add_to_selection_list(e)
    elif key=="N": # deselect all verts/sides/elements
      with SuspendRefresh():
        for l in self._selection_lists.values():
          l.clear_list()

  def enter_mode(self,old_mode):
    # at start-up, old_mode can be None
    old_selection_lists = old_mode._selection_lists \
      if old_mode is not None else {}
    # old_mode might use the same lists this mode
    if old_selection_lists is not self._selection_lists:
      with SuspendRefresh():
        for l in old_selection_lists.values():
          for entity in l:
            entity.deselect()
        for l in self._selection_lists.values():
          for entity in l:
            entity.select()

  def _new_vertex_for_the_el_builder(self,v):
    l = self._selection_lists[Vertex]
    if v in l: # trying to close the element
      if (l.size>2) and (v is l[0]): # element is defined correctly
        self.mc.add_new_element(l)
        l.clear_list()
    else:
      self.add_to_selection_list(v)


class ViewMode(Mode):
  
  __slots__ = ()

  def __init__(self,mc):
    super().__init__(mc)

  def mouse_click(self,xy,button):
    pass

  def object_pick(self,button,controller):
    pass

  def key_press(self,key):
    pass

  def enter_mode(self,old_mode):
    if old_mode is not None:
      # Use the selction lists of old_mode
      self._selection_lists = old_mode._selection_lists

# ----------------------------------------------------------------------

class MeshDesigner():

  def __init__(self):

    # 1) Create the model and GUI meshes
    self.mesh_mod = MeshModel()
    mesh_model_setPsi( Psi_default , gradPsi_default )
    SetupViewer()
    self.mesh_gui = MeshGUI()
    self.mc = Mesh( self.mesh_mod, self.mesh_gui.mv_bkend._mesh )

    # 1.1) define the controller modes
    self.GUI_modes = { \
       VertexMode : VertexMode( self.mc) , \
      ElementMode : ElementMode(self.mc) , \
         ViewMode : ViewMode(   self.mc) }
    self.GUI_mode_names = { \
       "vertex" : VertexMode  , \
      "element" : ElementMode , \
         "view" : ViewMode }
    self.GUI_mode = None

    # 1.2) make connections
    self.mesh_gui.set_callbacks(
      self.mouse_click, self.mouse_click_drag, \
      self.object_pick, self.mouse_scroll,     \
      self.key_press,   self.change_gui_mode,  \
      self.set_psi, self.unset_psi,            \
      self.set_lock_xy, self.unset_lock_xy,    \
      self.set_mu,      self.unset_mu,         \
      self.mesh_optimize,                      \
      self.mesh_refine,                        \
      self.vertex_editor_change_vdata ,        \
      self.side_editor_change_sdata )

    add_follower_Psi_change( self.mesh_gui.react2_notified_Psi_change )

    # 2) GUI mode selector
    self.GUI_modeX = None

    # 3) Track the last selected vertex
    self.last_selected_entity_monitor = { \
         key: None for key in EntityTypes }
    for mode in self.GUI_modes.values():
      for entity_type, react2 in zip( EntityTypes , \
        ( self.react2_notified_svlv_change ,        \
          self.react2_notified_sslv_change ,        \
          self.react2_notified_selv_change ) ):
        mode.add_follower_last_sel_entity_change( entity_type , react2 )

  def start(self):
    self.mesh_gui.activate(mesh_model_Psi,mesh_model_setPsi)
    self.mesh_gui.show()
    self.mesh_gui.cons_pushVariables( { \
                 "Psi":mesh_model_Psi   , \
              "setPsi":mesh_model_setPsi, \
     "mesh_controller":self             , \
         "import_mesh":self.import_mesh , \
         "export_mesh":self.export_mesh } )
    StartViewer()

  # Export/Import methods ----------------------------------------------

  def export_mesh(self):
    return self.mc.mm.export_mesh()

  def import_mesh(self,p,e,t):
    with SuspendRefresh():
      self.mc.mm.import_mesh(p,e,t)

  # Vertex/Side/Element selction monitor -------------------------------

  def react2_notified_svlv_change(self,v):
    # selected vertex - last vertex change
    if v is not None:

      # Create a monitor and register it to v. When v changes, the
      # monitor is destroyed, which also releases the follower. To
      # deal with delays in the garbage collector, in case an old
      # element still sends messages because its monitor is not
      # garbage collected, once the monitor detects a change the last
      # selected element is retrieved directly from the list, rather
      # than using the value communicated to the monitor. This ensures
      # that also spurious messages, despite being redundant, are
      # correct.
      class LastVertMonitor():
        # Note: this implementation resets the same data 3 times; it
        # could be improved using different functions for xy, psi and
        # lock, or having a general event v_change collecting all
        # element changes.
        def react2_notified_vdata_change(_,data):
          v = self.GUI_mode.get_last_in_selection_list(Vertex)
          if v is not None:
            self.mesh_gui.vertex_editor.update_vdata({ \
                 "id" : id(v.mv) , \
                 "xy" : v.mv.xy  , \
                "psi" : v.mv.psi , \
               "lock" : v.mv.lock \
              })

      mon = LastVertMonitor()
      v.mv.add_follower_xy_change(  mon.react2_notified_vdata_change)
      v.mv.add_follower_psi_change( mon.react2_notified_vdata_change)
      v.mv.add_follower_lock_change(mon.react2_notified_vdata_change)
      self.last_selected_entity_monitor[Vertex] = mon
    else:
      self.last_selected_entity_monitor[Vertex] = None
      self.mesh_gui.vertex_editor.clear_vdata()
  

  def react2_notified_sslv_change(self,s):
    if s is not None:

      class LastSideMonitor():
        def react2_notified_sdata_change(_,data):
          s = self.GUI_mode.get_last_in_selection_list(Side)
          if s is not None:
            self.mesh_gui.side_editor.update_sdata({ \
                 "id" : id(s.ms) , \
                 "mu" : s.ms.mu \
              })

      mon = LastSideMonitor()
      s.ms.add_follower_mu_change(mon.react2_notified_sdata_change)
      self.last_selected_entity_monitor[Side] = mon
    else:
      self.last_selected_entity_monitor[Side] = None
      self.mesh_gui.side_editor.clear_sdata()

  def react2_notified_selv_change(self,e):
    pass
  
  # Mode control -------------------------------------------------------

  def change_gui_mode(self,mode):
    old_mode = self.GUI_mode
    try:
      self.GUI_mode = self.GUI_modes[self.GUI_mode_names[mode]]
    except KeyError:
      raise ValueError('Unknown mode:',mode)
    self.GUI_mode.enter_mode(old_mode)
    # Trigger an update of the editors
    self.react2_notified_svlv_change( \
      self.GUI_mode.get_last_in_selection_list(Vertex) )
    self.react2_notified_sslv_change( \
      self.GUI_mode.get_last_in_selection_list(Side) )

  # Mouse click events -------------------------------------------------

  def mouse_click(self,xy,button):
    self.GUI_mode.mouse_click(xy,button)

  def mouse_click_drag(self,xy,dxy,button):
    if button==2:
      self.mc.vm.pan(dxy)

  # Object pick events -------------------------------------------------

  def object_pick(self,button,controller):
    self.GUI_mode.object_pick(button,controller)
    
  # Mouse scroll events ------------------------------------------------

  def mouse_scroll(self,xy,direction,nsteps):
    self.mc.vm.zoom(xy,nsteps)

  # Key press events ---------------------------------------------------

  def key_press(self,key):
    if key=="d" or key=="backspace": # remove the selected entities
      with SuspendRefresh():
        # Remove the elements first, to "free" the elements
        #
        # Notice: the selection lists chage while entities are
        # removed, so we make a copy.
        l = [e for e in self.GUI_mode.get_selected(Element)]
        for e in l:
          self.mc.remove_element(e)
        l = [v for v in self.GUI_mode.get_selected(Vertex)]
        for v in l:
          self.mc.remove_vertex(v)
    else: # mode specific operations
      self.GUI_mode.key_press(key)

  # Other GUI events ---------------------------------------------------

  def set_psi(self,psi):
    with SuspendRefresh():
      self.mc.set_psi_new_vertexes(psi)
      for v in self.GUI_mode.get_selected(Vertex):
        v.set_psi(psi)

  def unset_psi(self):
    self.set_psi(None)

  def set_lock_xy(self):
    with SuspendRefresh():
      for v in self.GUI_mode.get_selected(Vertex):
        v.set_lock(True)

  def unset_lock_xy(self):
    with SuspendRefresh():
      for v in self.GUI_mode.get_selected(Vertex):
        v.set_lock(False)

  def mesh_optimize(self,omega,n_iterations):
    with SuspendRefresh():
      max_dxy,_,_,_ = self.mc.mm.optimize_mesh(omega,n_iterations)
    return max_dxy

  def mesh_refine(self):
    with SuspendRefresh():
      self.mc.mm.refine_mesh()
    return len(self.mc.mm.v), len(self.mc.mm.s), len(self.mc.mm.e)

  def set_mu(self,mu):
    with SuspendRefresh():
      for s in self.GUI_mode.get_selected(Side):
        s.set_mu(mu)

  def unset_mu(self):
    self.set_mu(None)

  # Vertex/Side/Element editor events ----------------------------------

  def vertex_editor_change_vdata(self,vdata):
    v = self.GUI_mode.get_last_in_selection_list(Vertex)
    if v is not None:
      # The order is important: first lock, then psi, then xy
      v.mv.set_lock(vdata["lock"]==1)
      v.mv.set_psi( vdata["psi" ]   )
      v.mv.set_xy(  vdata["xy"  ]   )

  def side_editor_change_sdata(self,sdata):
    s = self.GUI_mode.get_last_in_selection_list(Side)
    if s is not None:
      s.ms.set_mu(sdata["mu"])


def main():
  mg = MeshDesigner()
  mg.start()

if __name__ == '__main__':
  main()
