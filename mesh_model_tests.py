## Copyright (C) 2016  Marco Restelli
##
## This file is part of: Poloidal Mesh Designer
##
## Poloidal Mesh is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3 of the
## License, or (at your option) any later version.
##
## Poloidal Mesh is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with the Poloidal Mesh source; If not, see
## <http://www.gnu.org/licenses/>.
##
## author: Marco Restelli                   <marco.restelli@gmail.com>


import unittest

import mesh_model as mm

#-----------------------------------------------------------------------

def check_no_attribute_dictionary(test,obj):
  """
  This function checks that trying to define a new attribute to an
  object raises an AttributeError exception. This is the expeted
  behaviour if __slots__ is used to suppress the attribute dictionary.
  """

  def raises_error(_obj):
    _obj.new_field_to_be_created = 12

  test.assertRaises( AttributeError , raises_error , obj )


class StateMonitor():
  """
  This class is useful to mock followers
  """
  def __init__(self):
    self.s = None
    self.n = 0
  def save_target_state(self,state):
    self.s = state
  def count_notifications(self,state):
    self.n += 1
    
#-----------------------------------------------------------------------

class FollowerCollections(unittest.TestCase):

  def test_state_follower_collection_has_no_attribute_dictionary(self):

    fc = mm.StateFollowerCollection()
    check_no_attribute_dictionary(self,fc)

  def test_action_follower_collection_has_no_attribute_dictionary(self):

    fc = mm.ActionFollowerCollection()
    check_no_attribute_dictionary(self,fc)

  def test_state_follower_is_notified_at_registration(self):

    sm = StateMonitor()
    fc = mm.StateFollowerCollection()
    fc.add_follower(sm.save_target_state,3)

    self.assertEqual( 3 , sm.s )

  def test_state_follower_is_notified_on_state_changes(self):

    sm = StateMonitor()
    fc = mm.StateFollowerCollection()
    fc.add_follower(sm.save_target_state,3)
    fc.notify_followers(5)

    self.assertEqual( 5 , sm.s )

  def test_action_follower_is_notified_on_state_changes(self):

    sm = StateMonitor()
    fc = mm.ActionFollowerCollection()
    fc.add_follower(sm.save_target_state)
    fc.notify_followers(5)

    self.assertEqual( 5 , sm.s )

  def test_dead_state_follower_is_removed(self):

    sm1 = StateMonitor()
    sm2 = StateMonitor()
    fc = mm.StateFollowerCollection()
    fc.add_follower(sm1.save_target_state,3)
    fc.add_follower(sm2.save_target_state,3)

    # Now there are two active followers
    self.assertEqual( len(fc._followers) , 2 )
    # Let us eliminate one follower
    sm1 = None
    # Clean-up happens during notification
    fc.notify_followers(5)
    self.assertEqual( len(fc._followers) , 1 )

  def test_dead_action_follower_is_removed(self):

    sm1 = StateMonitor()
    sm2 = StateMonitor()
    fc = mm.ActionFollowerCollection()
    fc.add_follower(sm1.save_target_state)
    fc.add_follower(sm2.save_target_state)

    # Now there are two active followers
    self.assertEqual( len(fc._followers) , 2 )
    # Let us eliminate one follower
    sm1 = None
    # Clean-up happens during notification
    fc.notify_followers(5)
    self.assertEqual( len(fc._followers) , 1 )

  def test_rm_follower(self):
      
    # there is no difference between state and action followers here
    sm1 = StateMonitor()
    sm2 = StateMonitor()
    fc = mm.ActionFollowerCollection()
    fc.add_follower(sm1.save_target_state)
    fc.add_follower(sm2.save_target_state)

    # Notify both followers with one value
    fc.notify_followers(5)
    # Remove one follower
    fc.rm_follower(sm1.save_target_state)
    # Notify the remaining follower with a different value
    fc.notify_followers(6)

    self.assertEqual( 1 , len(fc._followers) )
    self.assertEqual( 5 , sm1.s )
    self.assertEqual( 6 , sm2.s )

#-----------------------------------------------------------------------

class PsiFunctionGlobal(unittest.TestCase):
  """
  Test the global variable Psi and its notification lists.
  """

  def test_Psi_change_notification(self):

    sm = StateMonitor()
    mm.add_follower_Psi_change(sm.count_notifications)

    # being an action follower, sm is notified upon changes
    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )

    self.assertEqual( 1 , sm.n )

  def test_Psi_change_notification_allows_follower_removal(self):

    sm = StateMonitor()
    mm.add_follower_Psi_change(sm.count_notifications)

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    self.assertEqual( 1 , sm.n )

    mm.rm_follower_Psi_change(sm.count_notifications)
    mm.setPsi( lambda x,y: 2*(x**2+y**2) , \
               lambda x,y:    [4*x,4*y]  )
    self.assertEqual( 1 , sm.n )

#-----------------------------------------------------------------------

class Vertex(unittest.TestCase):

  def test_vertex_has_no_member_dictionary(self):

    v = mm.Vertex()
    check_no_attribute_dictionary(self,v)

  def test_vertex_lock_setter(self):

    v = mm.Vertex()

    for i in (0,1): # test all possible transitions
      v.set_lock(False)
      self.assertFalse( v.lock )

      v.set_lock(True)
      self.assertTrue( v.lock )

  def test_vertex_lock_change_notification(self):

    v = mm.Vertex()
    sm = StateMonitor()

    v.set_lock(False)
    # being a state follower, sm is notified also when it is added
    v.add_follower_lock_change(sm.save_target_state)
    self.assertFalse( sm.s )

    v.set_lock(True)
    self.assertTrue( sm.s )
    v.set_lock(False)
    self.assertFalse( sm.s )

  def test_vertex_lock_change_avoid_multiple_notification(self):
    v = mm.Vertex()
    sm = StateMonitor()

    v.set_lock(False)
    v.add_follower_lock_change(sm.count_notifications) # notify
    v.set_lock(False) # no notification
    v.set_lock(True)  # notify
    v.set_lock(True)  # no notification
    self.assertEqual( 2 , sm.n )

  def test_vertex_psi_setter_value(self):

    v = mm.Vertex()

    v.set_psi(12.5)
    self.assertEqual( 12.5 , v.psi )

    v.set_psi(-12.5)
    self.assertEqual( -12.5 , v.psi )

  def test_vertex_psi_change_notification(self):

    v = mm.Vertex()
    sm = StateMonitor()

    v.set_psi(None)
    v.add_follower_psi_change(sm.save_target_state)
    self.assertIsNone( sm.s )

    v.set_psi(12.5)
    self.assertEqual( 12.5 , sm.s )
    v.set_psi(None)
    self.assertIsNone( sm.s )

  def test_vertex_psi_change_avoid_multiple_notification(self):
    v = mm.Vertex()
    sm = StateMonitor()

    v.set_psi(None)
    v.add_follower_psi_change(sm.count_notifications) # notify
    v.set_psi(1.2) # notify
    v.set_psi(1.2) # no notification
    v.set_psi(1.3) # notify
    self.assertEqual( 3 , sm.n )

  def test_vertex_with_psi_is_notified_on_Psi_changes(self):

    # We need to override react2_notified_Psi_change to see whether it
    # is called and how many times.
    class VertexOverloadedMethod(mm.Vertex):
      def __init__(self):
        super().__init__()
        self.ncalls_react2_notified_Psi_change = 0
      def react2_notified_Psi_change(self,msg):
        self.ncalls_react2_notified_Psi_change += 1

    v = VertexOverloadedMethod()
    v.set_psi(12.5)
    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    self.assertEqual( 1 , v.ncalls_react2_notified_Psi_change )

  def test_vertex_without_psi_is_not_notified_on_Psi_changes(self):

    # See test_vertex_with_psi_is_notified_on_Psi_changes
    class VertexOverloadedMethod(mm.Vertex):
      def __init__(self):
        super().__init__()
        self.ncalls_react2_notified_Psi_change = 0
      def react2_notified_Psi_change(self,msg):
        self.ncalls_react2_notified_Psi_change += 1

    v = VertexOverloadedMethod()
    v.set_psi(12.5)
    v.set_psi(None)
    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    self.assertEqual( 0 , v.ncalls_react2_notified_Psi_change )

  def test_vertex_xy_setter_value(self):

    v = mm.Vertex()

    v.set_xy( [12.5 , -1] )
    self.assertEqual( 12.5 , v.xy[0] )
    self.assertEqual( -1.0 , v.xy[1] )

  def test_vertex_xy_setter_invalid_value_raises_exception(self):

    v = mm.Vertex()
    v.set_psi(1.2)
    self.assertRaises( ValueError , v.set_xy , [12.5] )
    self.assertRaises( ValueError , v.set_xy , [12.5,0.1,5.6] )

  def test_vertex_xy_change_notification(self):

    v = mm.Vertex()
    sm = StateMonitor()

    v.set_xy( [12.5 , -1] )
    v.add_follower_xy_change(sm.save_target_state)
    self.assertEqual( 12.5 , sm.s[0] )
    self.assertEqual( -1.0 , sm.s[1] )

    v.set_xy( [2.5 , 1.4] )
    self.assertEqual( 2.5 , sm.s[0] )
    self.assertEqual( 1.4 , sm.s[1] )

  def test_vertex_psi_setter_with_xy_resets_xy(self):

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    v = mm.Vertex()

    v.set_xy( [2.5 , -2.5] )
    # The point is now moved at 1/√2 , -1/√2
    v.set_psi( 1.0 )
    self.assertEqual( 1 , v.psi )
    self.assertAlmostEqual(  2.0**(-0.5) , v.xy[0] )
    self.assertAlmostEqual( -2.0**(-0.5) , v.xy[1] )

  def test_vertex_psi_setter_with_xy_and_lock(self):

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    v = mm.Vertex()

    v.set_xy( [2.5 , -2.5] )
    v.set_lock( True )
    # The point is not moved
    v.set_psi( 1.0 )
    self.assertEqual( 1 , v.psi )
    self.assertAlmostEqual(  2.5 , v.xy[0] )
    self.assertAlmostEqual( -2.5 , v.xy[1] )
    # Releasing the lock the point is free to move
    v.set_lock( False )
    self.assertEqual( 1 , v.psi )
    self.assertAlmostEqual(  2.0**(-0.5) , v.xy[0] )
    self.assertAlmostEqual( -2.0**(-0.5) , v.xy[1] )

  def test_vertex_psi_setter_with_xy_and_lock_followers(self):

    # Similar to test_vertex_psi_setter_with_xy_and_lock but we check
    # that the followers are notified.

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    v = mm.Vertex()

    sm_lock = StateMonitor()
    sm_psi  = StateMonitor()
    sm_xy   = StateMonitor()

    v.add_follower_lock_change(sm_lock.save_target_state)
    v.add_follower_psi_change( sm_psi.save_target_state )
    v.add_follower_xy_change(  sm_xy.save_target_state  )

    v.set_xy( [2.5 , -2.5] )
    v.set_lock( True )
    # The point is not moved
    v.set_psi( 1.0 )
    self.assertTrue( sm_lock.s )
    self.assertEqual( 1 , sm_psi.s )
    self.assertAlmostEqual(  2.5 , sm_xy.s[0] )
    self.assertAlmostEqual( -2.5 , sm_xy.s[1] )
    # Releasing the lock the point is free to move
    v.set_lock( False )
    self.assertFalse( sm_lock.s )
    self.assertEqual( 1 , sm_psi.s )
    self.assertAlmostEqual(  2.0**(-0.5) , sm_xy.s[0] )
    self.assertAlmostEqual( -2.0**(-0.5) , sm_xy.s[1] )

  def test_vertex_xy_setter_with_psi_and_lock(self):

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    v = mm.Vertex()

    v.set_xy( [2.5 , -2.5] )
    v.set_lock( True )
    v.set_psi( 1.0 )

    # Reset xy with lock: psi is ignored
    v.set_xy( [0 , 2.5] )
    self.assertAlmostEqual( 0   , v.xy[0] )
    self.assertAlmostEqual( 2.5 , v.xy[1] )

    v.set_lock( False )
    # Reset xy without lock: psi is used to correct xy
    v.set_xy( [2.5 , 0] )
    self.assertAlmostEqual( 1 , v.xy[0] )
    self.assertAlmostEqual( 0 , v.xy[1] )

  def test_vertex_with_psi_and_xy_resets_xy_if_Psi_reset(self):

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    v = mm.Vertex()

    v.set_xy( [2.5 , -2.5] )
    # The point is now moved at 1/√2 , -1/√2
    v.set_psi( 1.0 )

    # Reset Psi: the point moves to √2 , -√2
    mm.setPsi( lambda x,y: 0.25*(x**2+y**2) , \
               lambda x,y:     [0.5*x,0.5*y] )

    self.assertAlmostEqual(  2.0**(0.5) , v.xy[0] )
    self.assertAlmostEqual( -2.0**(0.5) , v.xy[1] )

  def test_vertex_with_psi_xy_lock_no_reset_xy_if_Psi_reset(self):

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )
    v = mm.Vertex()

    v.set_xy( [2.5 , -2.5] )
    # The point is now moved at 1/√2 , -1/√2
    v.set_psi( 1.0 )
    v.set_lock( True )

    # Reset Psi: the does not move
    mm.setPsi( lambda x,y: 0.25*(x**2+y**2) , \
               lambda x,y:     [0.5*x,0.5*y] )

    self.assertAlmostEqual(  2.0**(-0.5) , v.xy[0] )
    self.assertAlmostEqual( -2.0**(-0.5) , v.xy[1] )

#-----------------------------------------------------------------------

class Side(unittest.TestCase):

  def test_side_has_no_member_dictionary(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    s = mm.Side( (v1,v2) )
    check_no_attribute_dictionary(self,s)

  def test_side_constructor_raises_exception(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    v3 = mm.Vertex()
    self.assertRaises( ValueError , mm.Side , (v1,) )
    self.assertRaises( ValueError , mm.Side , (v1,v1) )
    self.assertRaises( ValueError , mm.Side , (v1,v2,v3) )

  def test_side_xy_change_notification(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()

    s = mm.Side( (v1,v2) )
    sm = StateMonitor()

    s.add_follower_xy_change(sm.save_target_state)

    v1.set_xy( [1.5 , -0.3] )
    # We don't know whether v1 is the first or the second vertex from
    # the side viewpoint; what we can test is that at least one vertex
    # must have the prescribed coordinates.
    self.assertTrue( any( \
      [ ( xy[0] == 1.5 ) and ( xy[1] == -0.3 ) for xy in sm.s ] ) )
    # set also the other vertex
    v2.set_xy( [0.5 , -2.3] )
    self.assertTrue( any( \
      [ ( xy[0] == 1.5 ) and ( xy[1] == -0.3 ) for xy in sm.s ] ) )
    self.assertTrue( any( \
      [ ( xy[0] == 0.5 ) and ( xy[1] == -2.3 ) for xy in sm.s ] ) )

  def test_side_mu_setter_value(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    s = mm.Side( (v1,v2) )

    s.set_mu( 8 )
    self.assertEqual( 8 , s.mu )

  def test_side_mu_setter_invalid_value_raises_exception(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    s = mm.Side( (v1,v2) )
    s.set_mu(2)

    self.assertRaises( ValueError , s.set_mu , None )
    self.assertRaises( ValueError , s.set_mu , 8.0  )
    self.assertRaises( ValueError , s.set_mu , -8   )

  def test_side_mu_change_avoid_multiple_notification(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    s = mm.Side( (v1,v2) )

    sm = StateMonitor()

    s.set_mu(2)
    s.add_follower_mu_change(sm.count_notifications) # notify
    s.set_mu(2) # no notification
    s.set_mu(3) # notify
    s.set_mu(3) # no notification
    self.assertEqual( 2 , sm.n )

#-----------------------------------------------------------------------

class Element(unittest.TestCase):

  def test_element_has_no_member_dictionary(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    v3 = mm.Vertex()
    e = mm.Element( (v1,v2,v3) )
    check_no_attribute_dictionary(self,e)

  def test_element_constructor_preserves_order(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    v3 = mm.Vertex()
    e = mm.Element( (v1,v2,v3) )
    self.assertTrue( e.v[0] is v1 )
    self.assertTrue( e.v[1] is v2 )
    self.assertTrue( e.v[2] is v3 )

  def test_element_constructor_raises_exception(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    self.assertRaises( ValueError , mm.Element , (v1,) )
    self.assertRaises( ValueError , mm.Element , (v1,v1) )
    self.assertRaises( ValueError , mm.Element , (v1,v1,v2) )

  def test_element_xy_change_notification(self):

    v1 = mm.Vertex()
    v2 = mm.Vertex()
    v3 = mm.Vertex()
    v4 = mm.Vertex()

    e = mm.Element( (v1,v2,v3,v4) )
    sm = StateMonitor()

    e.add_follower_xy_change(sm.save_target_state)

    v3.set_xy( [1.5 , -0.3] )
    self.assertEqual(  1.5 , sm.s[2][0] )
    self.assertEqual( -0.3 , sm.s[2][1] )

    v1.set_xy( [ 0.5 , 2.3] )
    v3.set_xy( [-1.5 , 0.3] )
    self.assertEqual(  0.5 , sm.s[0][0] )
    self.assertEqual(  2.3 , sm.s[0][1] )
    self.assertEqual( -1.5 , sm.s[2][0] )
    self.assertEqual(  0.3 , sm.s[2][1] )

#-----------------------------------------------------------------------

class MeshModel(unittest.TestCase):

  def test_add_new_vertex(self):

    mesh = mm.MeshModel()
    v = mesh.add_new_vertex( [0.2 , 1.3] )

    self.assertEqual( 1 , mesh.nv )
    self.assertTrue( v in mesh.v )
    self.assertEqual( 0.2 , v.xy[0] )
    self.assertEqual( 1.3 , v.xy[1] )

  def test_add_new_vertex_with_psi(self):

    mesh = mm.MeshModel()
    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )

    mesh.set_new_vert_psi( 1.0 )
    v = mesh.add_new_vertex( [0.5 , 0.5] )

    self.assertEqual( 1 , mesh.nv )
    self.assertTrue( v in mesh.v )
    self.assertAlmostEqual( 2.0**(-0.5) , v.xy[0] )
    self.assertAlmostEqual( 2.0**(-0.5) , v.xy[1] )

  def test_remove_vertex(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )

    # Remove the first vertex
    mesh.remove_vertex( v1 )
    self.assertEqual( 1 , mesh.nv )
    self.assertTrue( v2 in mesh.v )

  def test_remove_unknown_vertex_raises_exception(self):

    mesh = mm.MeshModel()
    v = mm.Vertex()
    self.assertRaises( KeyError , mesh.remove_vertex , v )

  def test_remove_vertex_with_connected_sides_does_nothing(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    s = mesh.add_new_side( (v1,v2) )

    # Removing the first vertex has no effect
    self.assertEqual( 2 , mesh.nv )
    mesh.remove_vertex( v1 )
    self.assertEqual( 2 , mesh.nv )
    self.assertTrue( v1 in mesh.v )
    self.assertTrue( v2 in mesh.v )

  def test_add_new_side__the_side_is_new(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )

    s = mesh.add_new_side( (v1,v2) )

    self.assertEqual( 1 , mesh.ns )
    self.assertTrue( s in mesh.s )
    self.assertTrue( v1 in s.v )
    self.assertTrue( v2 in s.v )

  def test_add_new_side__the_side_is_already_known(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )

    s = mesh.add_new_side( (v1,v2) )

    s = mesh.add_new_side( (v1,v2) ) # no new side is created
    self.assertEqual( 1 , mesh.ns )

    s = mesh.add_new_side( (v2,v1) ) # no new side is created
    self.assertEqual( 1 , mesh.ns )

  def test_add_new_side_notifies_the_vertexes(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )

    s1 = mesh.add_new_side( (v1,v2) )
    s2 = mesh.add_new_side( (v3,v2) )

    self.assertTrue( ( s1 in v1.s ) and ( s1 in v2.s ) )
    self.assertTrue( ( s2 in v3.s ) and ( s2 in v2.s ) )

  def test_add_new_side_with_unknown_verts_raises_exception(self):

    mesh = mm.MeshModel()
    v1 = mm.Vertex()
    v2 = mm.Vertex()

    self.assertRaises( ValueError , mesh.add_new_side , (v1,v2) )

  def test_remove_side(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    v3 = mesh.add_new_vertex( [0.2 , 0.3] )

    s1 = mesh.add_new_side( (v1,v2) )
    s2 = mesh.add_new_side( (v2,v3) )
    s3 = mesh.add_new_side( (v3,v1) )

    self.assertEqual( 3 , mesh.ns )
    mesh.remove_side( s2 )
    self.assertEqual( 2 , mesh.ns )
    mesh.remove_side( s1 )
    self.assertEqual( 1 , mesh.ns )
    mesh.remove_side( s3 )
    self.assertEqual( 0 , mesh.ns )

  def test_remove_side_vertexes_are_notified(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )

    s1 = mesh.add_new_side( (v1,v2) )
    s2 = mesh.add_new_side( (v3,v2) )

    self.assertTrue( ( s1 in v1.s ) and ( s1 in v2.s ) )
    self.assertTrue( ( s2 in v3.s ) and ( s2 in v2.s ) )

    mesh.remove_side( s2 )

    self.assertTrue( ( s2 not in v3.s ) and ( s2 not in v2.s ) )

  def test_remove_unknown_side_raises_exception(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )

    s = mm.Side( (v1,v2) )
    self.assertRaises( KeyError , mesh.remove_side , s )

  def test_add_new_element(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mesh.add_new_vertex( [0.5 , 1.6] )
    v4 = mesh.add_new_vertex( [1.4 , 2.5] )
    v5 = mesh.add_new_vertex( [2.4 , 3.5] )

    e_3v = mesh.add_new_element( (v1,v2,v3) )

    self.assertEqual( 1 , mesh.ne )
    self.assertTrue( e_3v in mesh.e )
    self.assertTrue( v1 in e_3v.v )
    self.assertTrue( v2 in e_3v.v )
    self.assertTrue( v3 in e_3v.v )

    e_4v = mesh.add_new_element( (v1,v2,v3,v4) )

    self.assertEqual( 2 , mesh.ne )
    self.assertTrue( e_4v in mesh.e )
    self.assertTrue( v1 in e_4v.v )
    self.assertTrue( v2 in e_4v.v )
    self.assertTrue( v3 in e_4v.v )
    self.assertTrue( v4 in e_4v.v )

    e_5v = mesh.add_new_element( (v1,v2,v3,v4,v5) )

    self.assertEqual( 3 , mesh.ne )
    self.assertTrue( e_5v in mesh.e )
    self.assertTrue( v1 in e_5v.v )
    self.assertTrue( v2 in e_5v.v )
    self.assertTrue( v3 in e_5v.v )
    self.assertTrue( v4 in e_5v.v )
    self.assertTrue( v5 in e_5v.v )

  def test_add_new_element_creates_new_sides(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )

    self.assertEqual( 0 , mesh.ns )

    e = mesh.add_new_element( (v1,v2,v3) )

    # Check that the three sides have been created
    self.assertEqual( 3 , mesh.ns )

  def test_add_new_element_with_known_sides_works(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )

    s = mesh.add_new_side( (v1,v2) )
    self.assertEqual( 1 , mesh.ns )
    self.assertEqual( 0 , len(s.e) )

    e = mesh.add_new_element( (v1,v2,v3) )

    self.assertEqual( 3 , mesh.ns )
    self.assertTrue( s in mesh.s ) # the side is still in the mesh
    self.assertTrue( e in s.e )    # the element has been notified

  def test_add_new_element_notifies_verts_and_sides(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )

    e = mesh.add_new_element( (v1,v2,v3) )

    self.assertTrue( ( e in v1.e ) and ( e in v2.e ) and ( e in v3.e ) )
    self.assertTrue( all([ e in s.e for s in mesh.s ]) )

  def test_add_new_element_with_unknown_verts_raises_exception(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mm.Vertex() # unknown vertex

    self.assertRaises( ValueError , mesh.add_new_element , (v1,v2,v3) )

  def test_remove_element(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    v3 = mesh.add_new_vertex( [0.2 , 0.3] )
    v4 = mesh.add_new_vertex( [0.8 , 0.8] )

    e1 = mesh.add_new_element( (v1,v2,v3) )
    e2 = mesh.add_new_element( (v1,v2,v4) )

    self.assertEqual( 2 , mesh.ne )
    mesh.remove_element( e2 )
    self.assertEqual( 1 , mesh.ne )
    mesh.remove_element( e1 )
    self.assertEqual( 0 , mesh.ne )

  def test_remove_element_removes_also_sides(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    v3 = mesh.add_new_vertex( [0.2 , 0.3] )
    v4 = mesh.add_new_vertex( [0.8 , 0.8] )

    e1 = mesh.add_new_element( (v1,v2,v3) )
    e2 = mesh.add_new_element( (v1,v2,v4) )

    self.assertEqual( 5 , mesh.ns )
    mesh.remove_element( e2 )
    self.assertEqual( 3 , mesh.ns )
    mesh.remove_element( e1 )
    self.assertEqual( 0 , mesh.ns )

  def test_remove_element_sides_and_vertexes_are_notified(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [0.4 , 1.5] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )

    e1 = mesh.add_new_element( (v1,v2,v3) )

    self.assertTrue( all([ e1 in v.e for v in mesh.v ]) )
    self.assertTrue( all([ e1 in s.e for s in mesh.s ]) )

    # add another element, so that some sides remain after rm e1
    v4 = mesh.add_new_vertex( [0.8 , 0.8] )
    e2 = mesh.add_new_element( (v1,v2,v4) )

    mesh.remove_element( e1 ) # rm e1 and some of the sides

    self.assertTrue( all([ e1 not in v.e for v in mesh.v ]) )
    self.assertTrue( all([ e1 not in s.e for s in mesh.s ]) )

  def test_remove_unknown_element_raises_exception(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )

    e = mm.Element( (v1,v2,v3) )
    self.assertRaises( KeyError , mesh.remove_element , e )

  def test_ExpImport_mesh__empty_mesh(self):

    mesh = mm.MeshModel()

    p,e,t = mesh.export_mesh()

    self.assertEqual( 0 , p.shape[1] )
    self.assertEqual( 0 , e.shape[1] )
    self.assertEqual( 0 , len(t) )

    mcpy = mm.MeshModel()
    mcpy.import_mesh(p,e,t)
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_ExpImport_mesh__mesh_with_1v(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )

    p,e,t = mesh.export_mesh()

    self.assertEqual( 1 , p.shape[1] )
    self.assertEqual( 0.2 ,      p[0,0]  )
    self.assertEqual( 1.3 ,      p[1,0]  )
    self.assertTrue( mm.np.isnan(p[2,0]) ) # no psi
    self.assertEqual(  0  ,      p[3,0]  ) # no lock

    self.assertEqual( 0 , e.shape[1] )
    self.assertEqual( 0 , len(t) )

    mcpy = mm.MeshModel()
    mcpy.import_mesh(p,e,t)
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

    v1.set_lock(True)
    p,e,t = mesh.export_mesh()
    self.assertEqual(  1  ,      p[3,0]  ) # lock

    mcpy = mm.MeshModel()
    mcpy.import_mesh(p,e,t)
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

    v1.set_psi(0.12345)
    p,e,t = mesh.export_mesh()
    self.assertEqual( 0.12345 ,  p[2,0]  ) # psi

    mcpy = mm.MeshModel()
    mcpy.import_mesh(p,e,t)
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_ExpImport_mesh__mesh_with_2v(self):

    mm.setPsi( lambda x,y: x**2+y**2 , \
               lambda x,y: [2*x,2*y] )

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 0.0] )
    v2 = mesh.add_new_vertex( [0.5 , 1.9] )

    v1.set_psi(2.0)
    v2.set_lock(True)

    p,e,t = mesh.export_mesh()

    self.assertEqual( 2 , p.shape[1] )
    self.assertEqual( 2.0**0.5 , p[0,0]  )
    self.assertEqual( 0.0 ,      p[1,0]  )
    self.assertEqual( 2.0 ,      p[2,0]  ) # psi
    self.assertEqual(  0  ,      p[3,0]  ) # no lock
    self.assertEqual( 0.5 ,      p[0,1]  )
    self.assertEqual( 1.9 ,      p[1,1]  )
    self.assertTrue( mm.np.isnan(p[2,1]) ) # no psi
    self.assertEqual(  1  ,      p[3,1]  ) # lock

    self.assertEqual( 0 , e.shape[1] )
    self.assertEqual( 0 , len(t) )

    mcpy = mm.MeshModel()
    mcpy.import_mesh(p,e,t)
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_ExpImport_mesh__mesh_with_3v_1e_1mu(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )
    s1 = mesh.add_new_side( (v1,v2) )
    e1 = mesh.add_new_element( (v1,v2,v3) )
    s1.set_mu( 3 )

    p,e,t = mesh.export_mesh()

    self.assertEqual( 3 , p.shape[1] )

    self.assertEqual( 1 , e.shape[1] )
    self.assertTrue( sorted([ ei for ei in e[:2,0] ]) == [1,2] )
    self.assertEqual( 3 , e[4,0] )

    self.assertEqual( 1 , len(t) )

    mcpy = mm.MeshModel()
    mcpy.import_mesh(p,e,t)
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_ExpImport_mesh__mesh_with_5v_1e_no_tri_quad(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    v3 = mesh.add_new_vertex( [0.2 , 1.5] )
    v4 = mesh.add_new_vertex( [0.7 , 0.7] )
    v5 = mesh.add_new_vertex( [0.9 , 1.0] )
    e1 = mesh.add_new_element( (v1,v2,v3,v4,v5) )

    p,e,t = mesh.export_mesh()

    self.assertEqual( 5 , p.shape[1] )

    self.assertEqual( 0 , e.shape[1] )

    # check that only one list (pentagons) is returned
    self.assertEqual( 1 , len(t) )

    mcpy = mm.MeshModel()
    mcpy.import_mesh(p,e,t)
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_refine_mesh__empty_mesh_does_not_change(self):
    
    mesh = mm.MeshModel()
    mesh.refine_mesh()

    mcpy = mm.MeshModel()
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_refine_mesh__only_vertexes_does_not_change(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [0.2 , 1.3] )
    v2 = mesh.add_new_vertex( [3.2 , 0.3] )
    mesh.refine_mesh()
    
    mcpy = mm.MeshModel()
    v1 = mcpy.add_new_vertex( [0.2 , 1.3] )
    v2 = mcpy.add_new_vertex( [3.2 , 0.3] )
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_refine_mesh__one_element(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [1.0 , 0.0] )
    v2 = mesh.add_new_vertex( [0.0 , 1.0] )
    v3 = mesh.add_new_vertex( [0.0 , 0.0] )
    e = mesh.add_new_element( (v1,v2,v3) )
    mesh.refine_mesh()
    
    mcpy = mm.MeshModel()
    v1 = mcpy.add_new_vertex( [1.0 , 0.0] )
    v2 = mcpy.add_new_vertex( [0.0 , 1.0] )
    v3 = mcpy.add_new_vertex( [0.0 , 0.0] )
    v4 = mcpy.add_new_vertex( [1.0/2.0 , 0.0    ] )
    v5 = mcpy.add_new_vertex( [0.0     , 1.0/2.0] )
    v6 = mcpy.add_new_vertex( [1.0/2.0 , 1.0/2.0] )
    e1 = mcpy.add_new_element( (v1,v6,v4) )
    e2 = mcpy.add_new_element( (v2,v5,v6) )
    e3 = mcpy.add_new_element( (v3,v4,v5) )
    e4 = mcpy.add_new_element( (v4,v6,v5) )
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_refine_mesh__one_element_and_independent_verts(self):
    
    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [1.0 , 0.0] )
    v2 = mesh.add_new_vertex( [0.0 , 1.0] )
    v3 = mesh.add_new_vertex( [0.0 , 0.0] )
    v4 = mesh.add_new_vertex( [1.0 , 1.0] )
    e = mesh.add_new_element( (v1,v2,v3) )
    mesh.refine_mesh()
    
    mcpy = mm.MeshModel()
    v1 = mcpy.add_new_vertex( [1.0 , 0.0] )
    v2 = mcpy.add_new_vertex( [0.0 , 1.0] )
    v3 = mcpy.add_new_vertex( [0.0 , 0.0] )
    v4 = mcpy.add_new_vertex( [1.0/2.0 , 0.0    ] )
    v5 = mcpy.add_new_vertex( [0.0     , 1.0/2.0] )
    v6 = mcpy.add_new_vertex( [1.0/2.0 , 1.0/2.0] )
    e1 = mcpy.add_new_element( (v1,v6,v4) )
    e2 = mcpy.add_new_element( (v2,v5,v6) )
    e3 = mcpy.add_new_element( (v3,v4,v5) )
    e4 = mcpy.add_new_element( (v4,v6,v5) )
    v7 = mcpy.add_new_vertex( [1.0 , 1.0] )
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_refine_mesh__many_elements(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [1.0 , 0.0] )
    v2 = mesh.add_new_vertex( [0.0 , 1.0] )
    v3 = mesh.add_new_vertex( [0.0 , 0.0] )
    v4 = mesh.add_new_vertex( [0.0 ,-1.0] )
    v5 = mesh.add_new_vertex( [1.0 ,-1.0] )
    e1 = mesh.add_new_element( (v1,v2,v3) )
    e2 = mesh.add_new_element( (v1,v3,v4,v5) )
    mesh.refine_mesh()
    
    mcpy = mm.MeshModel()
    v1  = mcpy.add_new_vertex( [1.0 , 0.0] )
    v2  = mcpy.add_new_vertex( [0.0 , 1.0] )
    v3  = mcpy.add_new_vertex( [0.0 , 0.0] )
    v4  = mcpy.add_new_vertex( [0.5 , 0.0] )
    v5  = mcpy.add_new_vertex( [0.0 , 0.5] )
    v6  = mcpy.add_new_vertex( [0.5 , 0.5] )
    v7  = mcpy.add_new_vertex( [0.0 ,-1.0] )
    v8  = mcpy.add_new_vertex( [0.5 ,-1.0] )
    v9  = mcpy.add_new_vertex( [1.0 ,-1.0] )
    v10 = mcpy.add_new_vertex( [0.0 ,-0.5] )
    v11 = mcpy.add_new_vertex( [0.5 ,-0.5] )
    v12 = mcpy.add_new_vertex( [1.0 ,-0.5] )

    e1 = mcpy.add_new_element( (v1,v6,v4) )
    e2 = mcpy.add_new_element( (v2,v5,v6) )
    e3 = mcpy.add_new_element( (v3,v4,v5) )
    e4 = mcpy.add_new_element( (v4,v6,v5) )
    e5 = mcpy.add_new_element( (v3,v4,v11,v10) )
    e6 = mcpy.add_new_element( (v4,v1,v12,v11) )
    e7 = mcpy.add_new_element( (v7,v8,v11,v10) )
    e8 = mcpy.add_new_element( (v9,v12,v11,v8) )
    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_optimize_mesh__one_iteration(self):

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [1.0 , 0.0] )
    v2 = mesh.add_new_vertex( [0.0 , 1.0] )
    v3 = mesh.add_new_vertex( [0.0 , 0.0] )
    v4 = mesh.add_new_vertex( [1/3 , 1/3] )
    e1 = mesh.add_new_element( (v1,v4,v3) )
    e2 = mesh.add_new_element( (v2,v4,v1) )
    e3 = mesh.add_new_element( (v3,v4,v2) )

    # Lock the corner nodes and perturb the central one
    v1.set_lock(True)
    v2.set_lock(True)
    v3.set_lock(True)
    v4.set_xy( (2,3) )

    mesh.optimize_mesh(1.0,1)
    
    mcpy = mm.MeshModel()
    v1 = mcpy.add_new_vertex( [1.0 , 0.0] )
    v2 = mcpy.add_new_vertex( [0.0 , 1.0] )
    v3 = mcpy.add_new_vertex( [0.0 , 0.0] )
    v4 = mcpy.add_new_vertex( [1/3 , 1/3] )
    e1 = mcpy.add_new_element( (v1,v4,v3) )
    e2 = mcpy.add_new_element( (v2,v4,v1) )
    e3 = mcpy.add_new_element( (v3,v4,v2) )
    v1.set_lock(True)
    v2.set_lock(True)
    v3.set_lock(True)

    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  def test_optimize_mesh__many_iterations(self):

    # Same as the one_iteration case, but taking omega<0 the
    # convergece takesplace only in the limit

    mesh = mm.MeshModel()
    v1 = mesh.add_new_vertex( [1.0 , 0.0] )
    v2 = mesh.add_new_vertex( [0.0 , 1.0] )
    v3 = mesh.add_new_vertex( [0.0 , 0.0] )
    v4 = mesh.add_new_vertex( [1/3 , 1/3] )
    e1 = mesh.add_new_element( (v1,v4,v3) )
    e2 = mesh.add_new_element( (v2,v4,v1) )
    e3 = mesh.add_new_element( (v3,v4,v2) )

    # Lock the corner nodes and perturb the central one
    v1.set_lock(True)
    v2.set_lock(True)
    v3.set_lock(True)
    v4.set_xy( (2,3) )

    mesh.optimize_mesh(0.9,15) # 15 iteration should be enough
    
    mcpy = mm.MeshModel()
    v1 = mcpy.add_new_vertex( [1.0 , 0.0] )
    v2 = mcpy.add_new_vertex( [0.0 , 1.0] )
    v3 = mcpy.add_new_vertex( [0.0 , 0.0] )
    v4 = mcpy.add_new_vertex( [1/3 , 1/3] )
    e1 = mcpy.add_new_element( (v1,v4,v3) )
    e2 = mcpy.add_new_element( (v2,v4,v1) )
    e3 = mcpy.add_new_element( (v3,v4,v2) )
    v1.set_lock(True)
    v2.set_lock(True)
    v3.set_lock(True)

    self.assertTrue( self.compare_meshes( mesh , mcpy ) )

  @staticmethod
  def compare_collections(test_equal,c1,c2):
    """
    Compare two (short) collections using strategy "test_equal". If an
    element is repeated in one collection, it must be repeated also in
    the second one.
    """

    # To ensure that all the elements in c2 have exactly one
    # correspondence in c1, we copy them and eliminate them once they
    # have been matched.
    c2c = [e for e in c2]
    for e1 in c1:
      found = False
      for e2 in c2c:
        if test_equal(e1,e2):
          found = True
          e2found = e2
          break
      if not found:
        return False
      else:
        c2c.remove( e2found )
    # We have matched all the verts in c1 - we have to check also c2
    if c2c: # c2c is not empty
      return False
    # If we are here, the two must be equal
    return True

  @staticmethod
  def compare_vertexes(v1,v2):
    toll = 1.0e-14
    if any( abs(v2.xy-v1.xy) > toll ): return False
    if    v2.lock != v1.lock:          return False
    if v2.psi is None:
      if v1.psi is not None:           return False
    else:
      if  abs(v2.psi-v1.psi) > toll :  return False
    return True

  @staticmethod
  def compare_sides(s1,s2):
    if not MeshModel.compare_collections( MeshModel.compare_vertexes , \
                                          s2.v, s1.v ): return False
    if s2.mu != s1.mu:                                  return False
    return True

  @staticmethod
  def compare_elements(e1,e2):
    if not MeshModel.compare_collections( MeshModel.compare_vertexes , \
                                          e2.v, e1.v ): return False
    if not MeshModel.compare_collections( MeshModel.compare_sides , \
                                          e2.s, e1.s ): return False
    return True

  @staticmethod
  def compare_meshes(m1,m2):

    if not MeshModel.compare_collections( MeshModel.compare_vertexes , \
      m2.v, m1.v ): print("\nVertex mismatch");  return False

    if not MeshModel.compare_collections( MeshModel.compare_sides , \
      m2.s, m1.s ): print("\nSide mismatch");    return False

    if not MeshModel.compare_collections( MeshModel.compare_elements , \
      m2.e, m1.e ): print("\nElement mismatch"); return False

    return True

#-----------------------------------------------------------------------

if __name__ == '__main__':
    unittest.main()

