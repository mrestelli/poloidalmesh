## Copyright (C) 2016  Marco Restelli
##
## This file is part of: Poloidal Mesh Designer
##
## Poloidal Mesh is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3 of the
## License, or (at your option) any later version.
##
## Poloidal Mesh is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with the Poloidal Mesh source; If not, see
## <http://www.gnu.org/licenses/>.
##
## author: Marco Restelli                   <marco.restelli@gmail.com>


import numpy as np
import matplotlib.patches as mpatches

"""
Matplotlib backend for the GUI module

This module deals with MatPlotLib and can be embedded in a larger GUI
application which can use a variety of frameworks, as far as they are
supported by MatPlotLib, i.e. as far as they define a "canvas" which
can be passed to the Mesh constructor.

Notice that this module can work with any of the matplotlib backends,
such as TkAgg and Qt4agg.

-- GUI refresh

In principle, whenever an entity is updated in the model, its
graphical counterpart should be updated too. To make the change
effective, one most methods in this module include a call to
refresh_plot.

Sometimes however it is more efficient to accumulate many changes and
then refresh them together. For this, one can suspend the refresh.
This can be done as

  with SuspendRefresh():
    <cluster many operations which update grid entities>

Graphical update will then take place on exit of the "with" block.

"""

# ----------------------------------------------------------------------

class SuspendRefresh():

  def __enter__(self):
    self._set_suspend_refresh(True)

  def __exit__(self, *args):
    self._set_suspend_refresh(False)

  def _set_suspend_refresh(self,suspend):
    global _suspend_refresh
    need_a_refresh = (not suspend) and _suspend_refresh
    _suspend_refresh = suspend
    if need_a_refresh: refresh_plot()
  
# Note: the refresh is handled with global variables; a reference to
# the active canvas is loaded here by the Mesh constructor. As far as
# there is only one figure, this simplifies things.
_suspend_refresh = False
_canvas_reference = None

def refresh_plot():
  """
  This is the function that should be called from all the methods in
  this module which update any graphical entity. The function itself
  takes care of suspending the refresh as appropriate.
  """
  if not _suspend_refresh:
    _canvas_reference.draw_idle()

# ----------------------------------------------------------------------

class Vertex():
  marker      = 'o'
  marker_psi  = 's'
  marker_lock = '*'
  markersize = 5

  __slots__ = 'has_psi', 'is_locked', 'is_selected', \
              'controller', 'artist', '__weakref__'

  # The vertex is constructed with default values and requires a call
  # to all the notification methods to acquire the proper look. This
  # allows reusing the notification methods for the initialization.
  def __init__(self,axes):
    artist = axes.plot([0,0] , zorder=10,picker=self._picker_size())[0]
    # The plot object is the artist which is retrieved by the event
    # handler. To be able to get to the Vertex from the artist we
    # store a reference to self in the artist itself.
    artist.gui_entity = self
    # fill the object
    self.has_psi     = False
    self.is_locked   = False
    self.is_selected = False
    self.controller  = None
    self.artist      = artist

  def erase(self):
    self.artist.remove()

  def set_controller(self,controller):
    self.controller = controller
  
  def _update_artist(self):
    if self.is_locked:
      self.artist.set_marker(self.marker_lock)
    else:
      if self.has_psi:
        self.artist.set_marker(self.marker_psi)
      else:
        self.artist.set_marker(self.marker)
    refresh_plot()

  def _set_default_look(self):
    self.artist.set_markeredgecolor('k')
    self.artist.set_markerfacecolor('k')
    self.artist.set_markersize(self.markersize)
    refresh_plot()

  def _set_selected_look(self):
    self.artist.set_markeredgecolor('g')
    self.artist.set_markerfacecolor('r')
    self.artist.set_markersize(1.5*self.markersize)
    refresh_plot()

  def react2_notified_xy_change(self,xy):
    self.artist.set_xdata([xy[0]])
    self.artist.set_ydata([xy[1]])
    refresh_plot()

  def react2_notified_psi_change(self,psi):
    self.has_psi = psi is not None
    self._update_artist()

  def react2_notified_lock_change(self,lock):
    self.is_locked = lock
    self._update_artist()

  def react2_notified_selected_change(self,selection):
    if selection:
      self.is_selected = True
      self._set_selected_look()
    else:
      self.is_selected = False
      self._set_default_look()

  def _picker_size(self):
    # Choose a reasonable size for the picker, given markersize
    ms = self.markersize
    c1 =  0.6 # linear scaling
    c2 = 25.0 # correction for small markers
    return c1*( ms + ( c2 / (ms+(2*c2**2)**(1.0/3.0)) )**2 )

# ----------------------------------------------------------------------

class Side():
  linewidth = 1

  __slots__ = 'mu', 'is_selected', '_color', '_alpha', '_linewidth', \
              'controller', 'artist', '__weakref__'

  def __init__(self,axes):
    artist = axes.plot( [0,0] , zorder=0,picker=2.5 )[0]
    artist.gui_entity = self
    # fill the object
    self.mu          = None
    self.is_selected = False
    self.controller  = None
    self.artist      = artist
    self._color      = np.ndarray((3,))
    self._alpha      = None
    self._linewidth  = None
    # complete initialization
    self._update_artist()

  def erase(self):
    self.artist.remove()

  def set_controller(self,controller):
    self.controller = controller

  def _set_default_look(self):
    self.artist.set_color(self._color)
    self.artist.set_alpha(self._alpha)
    self.artist.set_linewidth(self._linewidth)
    refresh_plot()

  def _set_selected_look(self):
    self.artist.set_color("r")
    self.artist.set_alpha(1.0)
    self.artist.set_linewidth(1.5*self._linewidth)
    refresh_plot()

  def _update_artist(self):
    if self.mu is None:
      self.artist.set_linestyle("-")
      self._color[...] = 0.0
      self._alpha      = 0.4
      self._linewidth  = self.linewidth
    else:
      self.artist.set_dashes([16,8])
      self._color[...] = self.rgb_random_color(self.mu)
      self._alpha      = 0.8
      self._linewidth  = 4.0*self.linewidth
    self.artist.set_color(self._color)
    self.artist.set_alpha(self._alpha)
    self.artist.set_linewidth(self._linewidth)
    if self.is_selected:
      self._set_selected_look()
    else:
      self._set_default_look()

  def react2_notified_xy_change(self,xy):
    self.artist.set_data(zip(*xy))
    refresh_plot()

  def react2_notified_mu_change(self,mu):
    self.mu = mu
    self._update_artist()

  def react2_notified_selected_change(self,selection):
    if selection:
      self.is_selected = True
      self._set_selected_look()
    else:
      self.is_selected = False
      self._set_default_look()

  @staticmethod
  def rgb_random_color(i):
    return (np.sin(10*np.array([1,2,3])+i)+1.0)/2.0

# ----------------------------------------------------------------------

class Element():
  facecolors = [ [0.5,1,0.3] , [1,0.3,0.5] , [0.3,0.5,1] ]
  linewidth  = 1
  alpha      = 0.3

  __slots__ = 'controller', 'artist', '__weakref__'

  def __init__(self,axes):
    artist = mpatches.Polygon([ [0,0] ],zorder=-10,picker=-10)
    axes.add_patch(artist)
    artist.gui_entity = self
    # fill the object
    self.controller = None
    self.artist     = artist

  def erase(self):
    self.artist.remove()

  def set_controller(self,controller):
    self.controller = controller
  
  def _set_default_look(self):
    nv = self.artist.get_xy().shape[0]-1 # first point is repeated
    self.artist.set_facecolor(self.facecolors[   \
      min( abs(nv-3) , len(self.facecolors)-1 ) ])
    self.artist.set_linewidth(self.linewidth)
    self.artist.set_alpha(self.alpha)
    self.artist.set_edgecolor("k")
    refresh_plot()

  def _set_selected_look(self):
    self.artist.set_facecolor([1,0.5,0])
    self.artist.set_alpha(0.8)
    self.artist.set_linewidth(2*self.linewidth)
    self.artist.set_edgecolor("c")
    refresh_plot()

  def react2_notified_xy_change(self,xy):
    if len(xy)>0:
      self.artist.set_xy(xy)
      refresh_plot()

  def react2_notified_selected_change(self,selection):
    if selection:
      self._set_selected_look()
    else:
      self._set_default_look()

# ----------------------------------------------------------------------

class Mesh():

  def __init__(self , fig,canvas):
          
    # Add the axes and connect the event handlers
    axes = fig.add_subplot(111)
    axes.set_aspect("equal")
    mouse_click_id  = canvas.mpl_connect( 'button_press_event', \
                                       self.mouse_click_handler )
    mouse_drag_id = [ canvas.mpl_connect( 'button_press_event', \
                                self.mouse_click_drag_handler_a ) , \
                      canvas.mpl_connect( 'button_release_event', \
                                self.mouse_click_drag_handler_b ) ]
    object_pick_id  = canvas.mpl_connect( 'pick_event', \
                                       self.object_pick_handler )
    mouse_scroll_id = canvas.mpl_connect( 'scroll_event', \
                                       self.mouse_scroll_handler)
    key_press_id    = canvas.mpl_connect( 'key_press_event', \
                                       self.key_press_handler )
    # store the references
    self.figure = fig
    self.canvas = canvas
    self.axes   = axes
    self.mouse_click_id  = mouse_click_id
    self.mouse_drag_id   = mouse_drag_id
    self.object_pick_id  = object_pick_id
    self.mouse_scroll_id = mouse_scroll_id
    self.key_press_id    = key_press_id

    self.new_vert_psi_contour = None

    # Register the canvas in the module variable
    global _canvas_reference
    _canvas_reference = self.canvas

  def set_callbacks(self, \
      mouse_click_callback , mouse_click_drag_callback, \
      object_pick_callback , mouse_scroll_callback,     \
      key_press_callback ):
    self.mouse_click_callback      = mouse_click_callback
    self.mouse_click_drag_callback = mouse_click_drag_callback
    self.object_pick_callback      = object_pick_callback
    self.mouse_scroll_callback     = mouse_scroll_callback
    self.key_press_callback        = key_press_callback

  def activate(self,Psi):
    self.Psi = Psi
    # this is a trick to "stabilize" the plot
    self.zoom( \
      [np.sum(self.axes.get_xlim())/2,np.sum(self.axes.get_ylim())/2],0)

  def get_fonts(self):
    return (self.axes.get_xticklabels()[0]).get_size()

  def set_fonts(self,size):
    for l in self.axes.get_xticklabels():
      l.set_size(size)
    for l in self.axes.get_yticklabels():
      l.set_size(size)
    refresh_plot()

  # Add/remove graphical entities --------------------------------------

  def add_new_vertex(self):
    v = Vertex(self.axes)
    refresh_plot()
    return v

  def remove_vertex(self,v):
    v.erase()
    refresh_plot()

  def add_new_side(self):
    s = Side(self.axes)
    refresh_plot()
    return s

  def remove_side(self,s):
    s.erase()
    refresh_plot()

  def add_new_element(self):
    e = Element(self.axes)
    refresh_plot()
    return e

  def remove_element(self,e):
    e.erase()
    refresh_plot()

  # General graphical utilities ----------------------------------------

  def set_axes_limits(self,xlim,ylim):
    self.axes.set_xlim( xlim )
    self.axes.set_ylim( ylim )
    refresh_plot()

  def zoom(self,xy,factor):
    # Zoom at coordinate xy; factor can be either positive or negative
    xlim = (1.0-0.075*factor)*(self.axes.get_xlim()-xy[0]) + xy[0]
    ylim = (1.0-0.075*factor)*(self.axes.get_ylim()-xy[1]) + xy[1]
    self.set_axes_limits( xlim , ylim )

  def pan(self,pan_vect):
    # Pan with vector pan_vect
    xlim = self.axes.get_xlim()-pan_vect[0]
    ylim = self.axes.get_ylim()-pan_vect[1]
    self.set_axes_limits( xlim , ylim )

  # Psi contour utilities ----------------------------------------------

  def plot_psi_countours(self,X,Y,psi_levs,style):
    artist = self.axes.contour(X,Y,self.Psi["Ψ"](X,Y),psi_levs,**style)
    refresh_plot()
    return artist
  
  def remove_psi_contour(self,artist):
    # seems that there is no better way than this...
    for c in artist.collections:
      c.remove()
    refresh_plot()

  # Mouse click events -------------------------------------------------

  def mouse_click_handler(self,event):
    """
    Send nonnull data to the controller.
    """
    x,y = event.xdata,event.ydata
    if (x is not None) and (y is not None):
      self.mouse_click_callback( \
        xy     = [event.xdata,event.ydata] , \
        button = event.button )

  def mouse_click_drag_handler_a(self,event):
    """
    Send nonnull data to the controller.

    Handle click-release combinations. This function registers the
    click event, while the corresponding "_b" functions completes the
    event.
    """
    x,y = event.xdata,event.ydata
    if (x is not None) and (y is not None):
      self.mouse_click_drag_handler_pressed = True
      self.mouse_click_drag_handler_x       = x
      self.mouse_click_drag_handler_y       = y
      self.mouse_click_drag_handler_button  = event.button

  def mouse_click_drag_handler_b(self,event):
    """
    Send nonnull data to the controller.

    This handles click-release combinations: it must be called both on
    click and release events, and only on release generates a
    callback.
    """
    x,y = event.xdata,event.ydata
    if (x is not None) and (y is not None):
      if self.mouse_click_drag_handler_pressed and \
         self.mouse_click_drag_handler_button == event.button:
        xlim = self.axes.get_xlim()
        ylim = self.axes.get_ylim()
        dxy = [ x-self.mouse_click_drag_handler_x , \
                y-self.mouse_click_drag_handler_y ]
        if (abs(dxy[0])/(xlim[1]-xlim[0]) > 0.01) and \
           (abs(dxy[1])/(ylim[1]-ylim[0]) > 0.01) :
          self.mouse_click_drag_callback( \
            xy     = [ self.mouse_click_drag_handler_x ,   \
                       self.mouse_click_drag_handler_y ] , \
            dxy    = dxy ,                                 \
            button = event.button )
    self.mouse_click_drag_handler_pressed = False

  def object_pick_handler(self,event):
    """
    Send button press events to the controller
    """
    if event.mouseevent.name=="button_press_event":
      self.object_pick_callback( \
            button = event.mouseevent.button ,          \
        controller = event.artist.gui_entity.controller )

  # Mouse scroll events ------------------------------------------------

  def mouse_scroll_handler(self,event):
    """
    Send nonnull position and scroll to the controller.
    """
    directions = {"up":"Up" , "down":"Down"}
    x,y = event.xdata,event.ydata
    if (x is not None) and (y is not None):
      self.mouse_scroll_callback( \
        xy        = [event.xdata,event.ydata] , \
        direction = directions[event.button] , \
        nsteps    = event.step )

  # Key press events ---------------------------------------------------

  def key_press_handler(self,event):
    """
    Send the key to the controller
    """
    self.key_press_callback( key = event.key )

# ----------------------------------------------------------------------

if __name__ == "__main__":

  def cb1(xy,button):
    print("Callback 1: xy=",xy,", button=",button)
  def cb2(button,controller):
    print("Callback 2: button=",button,", controller=",controller)
  def scb(xy,direction,nsteps):
    print("Scroll callback 2: ",xy,direction,nsteps)
  def key_cb(key):
    print('Key pressed: "'+key+'"')

  import matplotlib.pyplot

  # Generate the figure and the canvas, using the matplotlib defaults
  fig = matplotlib.pyplot.figure()
  canvas = fig.canvas

  # Generate and activate the mesh viewer
  mesh = Mesh(fig,canvas)
  mesh.set_callbacks(cb1,cb2,scb,key_cb)
  mesh.activate( Psi = {"Ψ" : (lambda x,y : x**2 + y**3)} )

  # Add some vertexes
  v1 = mesh.add_new_vertex()
  v1.react2_notified_xy_change([0.5,0.6])
  v1.react2_notified_selected_change(False)
  v1.react2_notified_psi_change(None)
  v1.set_controller("A")
  v2 = mesh.add_new_vertex()
  v2.react2_notified_xy_change([-0.5,-0.5])
  v2.react2_notified_selected_change(False)
  v2.react2_notified_psi_change(None)
  v2.set_controller("B")

  # Add some sides
  s1 = mesh.add_new_side()
  s1.react2_notified_xy_change([[-0.5,1],[0.3,1.5]])
  s1.react2_notified_mu_change(3)
  s1.react2_notified_selected_change(False)
  s1.set_controller("Side controller s1")
  s2 = mesh.add_new_side()
  s2.react2_notified_xy_change([[0.4,1.6],[0.6,-0.2]])
  s2.react2_notified_selected_change(True)
  s2.set_controller("Side controller s2")

  # Add some elements
  e1 = mesh.add_new_element()
  e1.react2_notified_xy_change([[0,0],[0.5,0],[0,0.5]])
  e1.react2_notified_selected_change(False)
  e1.set_controller("El. controller K")
  e2 = mesh.add_new_element()
  e2.react2_notified_xy_change([[-1,-1],[-0.5,-1],[-0.6,-0.7],[-0.8,-0.5]])
  e2.react2_notified_selected_change(False)
  e2.set_controller("El. controller Q")

  # Change some settings
  v1.react2_notified_xy_change([0.3,0.9])
  v1.react2_notified_lock_change(True)
  v2.react2_notified_selected_change(True)
  e2.react2_notified_selected_change(True)

  # Add some countour lines
  X, Y = np.meshgrid( np.linspace(-1.5,1.5,100) , np.linspace(-2,2,100) )
  mesh.plot_psi_countours(X,Y,(0,),{"linewidths":(3,)})
  mesh.plot_psi_countours(X,Y,(-0.2,0.2,0.4,0.5),{"linewidths":(1,)})

  # Reset the axes
  mesh.set_axes_limits([-1.5,1.5],[-2,2])

  matplotlib.pyplot.show()

